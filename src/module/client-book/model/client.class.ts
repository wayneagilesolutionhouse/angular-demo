﻿
import { Contact } from './contact.class';
import { Address } from './address.class';
import { GlobalFunctions } from '../../Global/global-function.constant';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ModelBase } from '../../Global/model/model-base.class';
import { isNullOrUndefined } from 'util';

export class Client extends ModelBase {

    public _contact: Contact;
    public _address: Address;

    constructor(id: number, contact: Contact, address: Address) {
        super();
        this.name = !isNullOrUndefined(contact) ? GlobalFunctions.valid(contact.firstname)
         + ' ' + GlobalFunctions.valid(contact.surname) : '';
        this.id = id;
        this.contact = contact;
        this.address = address;
        this.path = 'ClientBook/clients';
    }

    get address(): Address {
        return this._address;
    }
    set address(value: Address) {
        this._address = value;
    }

    get contact(): Contact {
        return this._contact;
    }
    set contact(value: Contact) {
        this._contact = value;
    }

    static instantiate() {
        const address = Address.instantiate('', '', '', '', null);
        const contact = Contact.instantiate('', '', '', '');
        return new Client(0, contact, address);
    }

    static mapFormToModel = (complexForm: FormGroup, id: number, addressId: number, contactId: number): Client => {
      const entity = Client.instantiate();
      return entity.mapFormToModel(complexForm, id, addressId, contactId);
    }

    static mapModelToServiceModel(model: Client): any {
      const entity = Client.instantiate();
      return entity.mapModelToServiceModel(model);
    }

    static mapServiceModelToModel(serviceModel: any) {
      const entity = Client.instantiate();
      return entity.mapServiceModelToModel(serviceModel);
    }

    public mapModelToServiceModel(model: Client): any {
      return GlobalFunctions.isNullOrUndefined(model) ? null : {
          Name: model.name,
          ID: model.id,
          Active: !model.markForDeletion,
          Contact: Contact.mapModelToServiceModel(model.contact),
          ContactId: model.contact.id,
          Address: Address.mapModelToServiceModel(model.address),
          AddressId: model.address.id
      };
    }

    public mapServiceModelToModel(serviceModel: any): Client {
        const id = !GlobalFunctions.isNullOrUndefined(serviceModel) &&
        !GlobalFunctions.isNullOrUndefined(serviceModel.id) ? serviceModel.id : 0;

        const contact = !GlobalFunctions.isNullOrUndefined(serviceModel) &&
        !GlobalFunctions.isNullOrUndefined(serviceModel.contact) ? Contact.mapServiceModelToModel(serviceModel.contact) : null;

        const address = !GlobalFunctions.isNullOrUndefined(serviceModel) &&
        !GlobalFunctions.isNullOrUndefined(serviceModel.address) ? Address.mapServiceModelToModel(serviceModel.address) : null;

        const client = new Client(id, contact, address);
        client.markForDeletion = !serviceModel.active;
        return client;
    }

  public mapServiceModelsToModels(servicesModel: any) {
    const returnValue: Client[] = [];

    if (Array.isArray(servicesModel)) {
        servicesModel.forEach(serviceModel => {
            returnValue.push(this.mapServiceModelToModel(serviceModel));
        });
    }
    return returnValue;
  }

  public mapFormToModel = (complexForm: FormGroup, id: number, addressId: number, contactId: number): Client => {
      addressId = (<FormGroup> complexForm.controls['address']).controls['id'].value;
      contactId = (<FormGroup> complexForm.controls['contact']).controls['id'].value;
      const contact = Contact.mapFormToModel(<FormGroup> complexForm.controls['contact'], contactId);
      const address = Address.mapFormToModel(<FormGroup> complexForm.controls['address'], addressId);
      const client = new Client(id, contact, address);

      return client;
  }
}
