import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { LocalStorageService } from 'angular-2-local-storage';
import { GlobalFunctions } from '../../Global/global-function.constant';
import { AuthenticationService } from '../service/authentification.service';
import { User } from '../model/user';
// import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { NgxService } from '../../Global/service/ngx.service';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private router: Router,
      private authenticationService: AuthenticationService,
      private localStorageService: LocalStorageService // ,
     // private spinnerService: Ng4LoadingSpinnerService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      console.log('canactivate');
     //  this.spinnerService.show();
     try {
      this.authenticationService.loginWithBearer().subscribe(
        user => {
        //  this.spinnerService.hide();
          // const user = User.mapServiceLoginModelToModel(this.localStorageService.get('currentUser'));
          if (GlobalFunctions.isNullOrUndefined(user)) {
            this.router.navigate(['/authentification/login']);
          } else {
              this.localStorageService.set('currentUser', JSON.stringify(user));
          }
        },
        error => {
          this.router.navigateByUrl('/login');
        });

      return true;
     } catch (error) {
      this.router.navigateByUrl('/login');
     }
    }
}
