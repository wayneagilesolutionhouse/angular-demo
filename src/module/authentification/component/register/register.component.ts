import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../../model/user';
import { UserService } from '../../service/user.service';
import { AlertService } from '../../service/alert.service';

@Component({
    templateUrl: 'register.component.html'
})

export class RegisterComponent {
    model: User;
    loading = false;

    constructor(
        private router: Router,
        private userService: UserService,
        private alertService: AlertService) {
        this.model = new User();
    }

    public register() {
        this.loading = true;
        this.model.token = 'token';
        this.model.userTypeId = 2;
        this.userService.create(this.model)
            .subscribe(
            data => {
                this.alertService.success('Registration successful', true);
                this.router.navigate(['/authentification/login']);
            },
            error => {
                this.alertService.error(error);
                this.loading = false;
            });
    }
}
