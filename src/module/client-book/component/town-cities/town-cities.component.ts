﻿import { Component, OnInit } from '@angular/core';
import { Address } from '../../model/address.class';
import { CityTown } from '../../model/city-town.class';
import { Country } from '../../model/country.class';
import { CityTownComponent } from './town-city/town-city.component';
import { DialogService } from 'ng2-bootstrap-modal';
import { TownService } from '../../service/town.service';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'app-town-cities',
    templateUrl: './town-cities.component.html'
})
export class TownCitiesComponent implements OnInit {
    public cityTowns: CityTown[];
    private ignoreClick: boolean;

    constructor(private dialogService: DialogService, private townService: TownService) {
    }

    ngOnInit() {
      this.cityTowns = [];
      this.townService.getAll().subscribe(x => {
          this.cityTowns = x;
      });
    }

    public showDetails(selected: CityTown) {
        if (!this.ignoreClick) {
            const self = this;
            const disposable = this.dialogService.addDialog(CityTownComponent, {
                title: 'CityTown',
                cityTown: selected
            })
            .subscribe((isConfirmed) => {
                if (isConfirmed) {
                    self.townService.getAll().subscribe(x => {
                      this.cityTowns = x;
                    });
                }
            });
        }
    }

    public openCreate = () => {
        this.showDetails(new CityTown(''));
    }

    public checkBoxOver() {
        this.ignoreClick = true;
    }

    public checkBoxOut() {
        this.ignoreClick = false;
    }

    public deleteAll = () => {
      const self = this;
      self.townService.deleteAll(this.cityTowns).subscribe(x => {
        self.townService.getAll().subscribe(towns => {
          this.cityTowns = towns;
        }, e => {
          console.log(e);
        });
      });
    }

    public isMarkedForDeletion(): boolean {
      let returnValue: boolean;
      returnValue = this.townService.markForDeletion(this.cityTowns);
      return returnValue;
    }
}
