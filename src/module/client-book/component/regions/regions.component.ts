﻿import { Component, OnInit } from '@angular/core';
import { Region } from '../../model/region.class';
import { RegionComponent } from './region/region.component';
import { DialogService } from 'ng2-bootstrap-modal';
import { RegionService } from '../../service/region.service';
import { Observable } from 'rxjs/Observable';
import { isNullOrUndefined } from 'util';

@Component({
    selector: 'app-regions',
    templateUrl: './regions.component.html'
})
export class RegionsComponent implements OnInit {
    public regions: Region[];
    public selection: Region;
    public show: boolean;
    private ignoreClick: boolean;

    constructor(private dialogService: DialogService, private regionService: RegionService) {
        // const regions = regionService.getAll();
        // this.regions = Observable.of(regions);
    }

    ngOnInit() {
      this.regions = [];
      this.regionService.getAll().subscribe(x => {
          this.regions = x;
      });
    }

    public showDetails(selected: Region) {
        if (!this.ignoreClick) {
            const disposable = this.dialogService.addDialog(RegionComponent, {
                title: 'Region',
                region: selected
            })
                .subscribe((isConfirmed) => {
                  this.regions = [];
                  this.regionService.getAll().subscribe(x => {
                      this.regions = x;
                  });
                });
        }
    }

    public openCreate = () => {
        this.showDetails(new Region(0, '' , null));
    }

    public checkBoxOver() {
        this.ignoreClick = true;
    }

    public checkBoxOut() {
        this.ignoreClick = false;
    }

    public deleteAll = () => {
      const self = this;
      self.regionService.deleteAll(this.regions).subscribe(x => {
        this.regionService.getAll().subscribe(regions => {
          this.regions = regions;
        }, e => {
          console.log(e);
        });
      });
    }

    public compareFn(option, selected): boolean {
      return !isNullOrUndefined(option) && !isNullOrUndefined(selected) ?
        option.id === selected.id : false;
    }

    public isMarkedForDeletion(): boolean {
      let returnValue: boolean;
      returnValue = this.regionService.markForDeletion(this.regions);
      return returnValue;
    }
}
