import { GridHeaderModel } from './grid-header.model';

export class GridSort {
    constructor(public key: string, public sortOrder: string) {}
    public getSortDescription(field: GridHeaderModel): string {
        if (field.propertyName === this.key) {
            this.sortOrder = this.sortOrder !== 'asc' ? 'asc' : 'desc';
        } else {
            this.key = field.propertyName;
            this.sortOrder = 'asc';
        }

        // Serialise data
        return this.key + '-' + this.sortOrder + '-' + field.type;
    }
}
