﻿
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GlobalFunctions } from '../../Global/global-function.constant';
import { ModelBase } from '../../Global/model/model-base.class';

export class Contact extends ModelBase {
    constructor(name: string) {
        super();
        this.firstname = name;
        this.path = 'ClientBook/contacts';
    }
    public firstname: string;
    public surname: string;
    public telephone: string;
    public mobile: string;
    public email: string;
    public website: string;
    public linkedIn: string;
    public skype: string;

    static instantiate = (firstname: string, surname: string, telephone: string, email: string) => {
        const returnValue = new Contact(firstname);
        returnValue.surname = surname;
        returnValue.telephone = telephone;
        returnValue.email = email;
        return returnValue;
    }

  static mapModelToServiceModel(model: Contact): any {
    const entity = new Contact('');
    return entity.mapModelToServiceModel(model);
  }

  static mapServiceModelToModel(serviceModel: any): Contact {
    const entity = new Contact('');
    return entity.mapServiceModelToModel(serviceModel);
  }

  static mapServiceModelsToModels(servicesModel: any) {
    const entity = new Contact('');
    return entity.mapServiceModelsToModels(servicesModel);
  }

  static mapFormToModel(complexForm: FormGroup, id: number): Contact {
    const entity = new Contact('');
    return entity.mapFormToModel(complexForm, id);
  }

  public mapFormToModel(complexForm: FormGroup, id: number): Contact {
    const firstname = !GlobalFunctions.isNullOrUndefined(complexForm.get('firstname').value) ? complexForm.get('firstname').value : null;
    const surname = !GlobalFunctions.isNullOrUndefined(complexForm.get('surname').value) ? complexForm.get('surname').value : null;
    const telephone = !GlobalFunctions.isNullOrUndefined(complexForm.get('telephone').value) ? complexForm.get('telephone').value : null;
    const email = !GlobalFunctions.isNullOrUndefined(complexForm.get('email').value) ? complexForm.get('email').value : null;

    const contact = Contact.instantiate(firstname, surname, telephone, email);
    contact.mobile = !GlobalFunctions.isNullOrUndefined(complexForm.get('mobile').value) ? complexForm.get('mobile').value : null;
    contact.website = !GlobalFunctions.isNullOrUndefined(complexForm.get('website').value) ? complexForm.get('website').value : null;
    contact.skype = !GlobalFunctions.isNullOrUndefined(complexForm.get('skype').value) ? complexForm.get('skype').value : null;
    contact.id = id;
    return contact;
  }

  public mapModelToServiceModel(model: Contact): any {
    return GlobalFunctions.isNullOrUndefined(model) ? null : {
        Name: model.name,
        ID: model.id,
        Active: !model.markForDeletion,
        FirstName: model.firstname,
        Mobile: model.mobile,
        Telephone: model.telephone,
        Surname: model.surname,
        Email: model.email,
        Website: model.website,
        Skype: model.skype
    };
  }

  public mapServiceModelToModel(serviceModel: any): Contact {
      const contact = new Contact(serviceModel.name);
      contact.id = serviceModel.id;
      contact.firstname = serviceModel.firstName;
      contact.surname = serviceModel.surname;
      contact.email = serviceModel.email;
      contact.skype = serviceModel.skype;
      contact.website = serviceModel.website;
      contact.markForDeletion = !serviceModel.active;
      contact.telephone = serviceModel.telephone;
      contact.mobile = serviceModel.mobile;
      return contact;
  }

  public mapServiceModelsToModels(servicesModel: any) {
    const returnValue: Contact[] = [];

    if (Array.isArray(servicesModel)) {
        servicesModel.forEach(serviceModel => {
            returnValue.push(this.mapServiceModelToModel(serviceModel));
        });
    }
    return returnValue;
  }
}
