import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from '../../service/alert.service';
import { AuthenticationService } from '../../service/authentification.service';
import { GlobalFunctions } from '../../../global/global-function.constant';
import { LocalStorageService } from 'angular-2-local-storage';
import { isArray } from 'util';

@Component({
    templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit {
    model: any = {};
    loading = false;
    public errorMessage: string;
    public loginFailed = false;
    returnUrl: string;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private localStorageService: LocalStorageService
        ) { }

    ngOnInit() {
        // reset login status
        this.authenticationService.logout();

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '';
    }

    login() {
        this.loading = true;
        this.loginFailed = false;
        this.authenticationService.login(this.model.username, this.model.password)
          .subscribe(
          user => {
            if (GlobalFunctions.isNullOrUndefined(user)) {
                this.loading = false;
                this.loginFailed = true;
            } else {
                this.localStorageService.set('currentUser', JSON.stringify(user));
                this.router.navigate(['home']);
            }
          },
          error => {
              // this.alertService.error(error);
              if (isArray(error)) {
                this.errorMessage = error[0].message;
              } else {
                this.errorMessage = 'Please contact Administrator';
              }
              this.loginFailed = true;
              this.loading = false;
          });
    }
}

