# Busy Indicator
The lights are on and somebody's home.

There are two component to choose from dependent on your needs.

## Attribute Component
Where you have a live child area. This will add the applicable `aria-*` 
attributes.

```angular2html
<div [im2Busy]="someBooleanOrComponent">
    <div *ngIf="let foo of someObservable | async">
        <!-- some template -->
    </div>
</div>
```

This will mark up the outer div with ARIA attributes and show/hide the 
children as `someBoolean` changes. It will add a loading spinner as a child 
which will also show/hide appropriately.

The input value can be a boolean, or a component which implements the 
`ExposeLoading` interface.

### Custom spinner template
In such cases where the loading spinner must conform to some template (such 
as where the component is on a `tbody` element), you can specify a template.

```angular2html
<tbody [im2Busy]="someBooleanOrComponent">
    <tr #im2Busy>
        <td colspan="4" #spinner></td>
    </tr>
    <tr *ngIf="let foo of someArray">
        <!-- some template with tds -->
    </tr>
</tbody>
```

The component will search for an element `#im2Busy` and use that instead of 
the default `<div>`. It will add the appropriate class names so that the CSS 
rules and mixins still match.

If there is a `#spinner` element, the spinner glyph will be added to it as a 
child.  If there is no `#spinner`, the glyph will be added to the `#im2Busy` 
element.

### Mixins
To style your busy indicator there is a mixin to help.

 * `im2-busy()`
```sass
@import "shared/busy-indicator/busy-indicator.mixin.scss";

:host {
    @include im2-busy {
        color: red;
    }
    
    // custom template example
    @include im2-busy {
        td {
            color: red;
        }
    }
}
```

The CSS block in the mixin is scoped to the `.im2-busy` element.