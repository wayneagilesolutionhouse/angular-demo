﻿import { Component, OnInit } from '@angular/core';
import { Contact } from '../../model/contact.class';
import { ContactSmartComponent } from './contact-smart/contact-smart.component';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { Observable } from 'rxjs/Observable';
import { GridHeaderModel } from '@global/component/grid/grid-header.model';
import { of } from 'rxjs/observable/of';
import { GenericsComponent } from '@global/component/generic/generics.component';
import { GenericService } from '@global/service/generic.service';

@Component({
    selector: 'app-contacts',
    templateUrl: './contacts.component.html'
})

export class ContactsComponent extends GenericsComponent<Contact, ContactSmartComponent> implements OnInit {
  public isBusy: boolean;
  public contactHeaders: GridHeaderModel[];
  public headerTranslation: {};
  constructor(public dialogService: DialogService, public contactService: GenericService) {
    super(new Contact(''), dialogService, contactService);
    this.contactHeaders =  [
      GridHeaderModel.CreateTextHeader('firstname', 'firstname', true),
      GridHeaderModel.CreateTextHeader('surname', 'surname', true),
      GridHeaderModel.CreateTextHeader('telephone', 'telephone', true),
      GridHeaderModel.CreateTextHeader('email', 'email', true),
      GridHeaderModel.CreateDeleteHeader('id', 'id', false),
    ];
    // Mocked translations
    this.headerTranslation = {
      'firstname': 'Firstname',
      'surname': 'Surname',
      'telephone': 'Telephone',
      'email': 'Email',
      'id': ''
    };
    this.isBusy = true;
  }

  ngOnInit() {
    this.isBusy = true;
    this.contactService.getAll(new Contact('')).subscribe(x => {
      this.data = of(x);
      this.isBusy = false;
    });
  }

  public openCreate = () => {
      this.showDetails(this.entity, this.dialogService.addDialog(ContactSmartComponent, {
        title: 'Contact',
        entity: this.entity
    }) );
  }

  public show(contact: Contact) {
      this.showDetails(contact, this.dialogService.addDialog(ContactSmartComponent, {
        title: 'Contact',
        entity: contact
      }) );
  }
}
