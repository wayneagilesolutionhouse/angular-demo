import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// Components
import { LoginComponent } from './component/login/login.component';
import { RegisterComponent } from './component/register/register.component';
// Guards
import { AuthGuard } from './guard/auth.gaurd';

const routes: Routes = [
  // { path: '', component: LoginComponent , canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent},
  { path: 'register', component: RegisterComponent},
//  { path: '/', redirectTo: ''}
];

export const authenticationRouting: ModuleWithProviders = RouterModule.forChild(routes);
