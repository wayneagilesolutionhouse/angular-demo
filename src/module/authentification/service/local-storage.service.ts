﻿import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Observable } from 'rxjs/rx';
import { Subject } from 'rxjs/Subject';
import { GlobalFunctions } from '../../Global/global-function.constant';

@Injectable()
export class LocalStorageService {
    private subject = new Subject<any>();
    private keepAfterNavigationChange = false;
    private mockLocalStorage = {};

    constructor() {
            this.mockLocalStorage = {
            currentUser: 'Wayne Porter'
        };
    }

    public setItem(itemName: string, jsonUser: string) {
        try {
            if (GlobalFunctions.isNullOrUndefined(localStorage)) {
               // return localStorage.setItem(itemName, jsonUser);
            } else {
                this.mockLocalStorage[itemName] = jsonUser;
            }
        } catch (ex) {
            throw new Error('Local storage item does not exist');
        }
    }

    public getItem(item: string): string {
      if (!GlobalFunctions.isNullOrUndefined(localStorage)) {
          return localStorage.getItem(item);
      } else {
        try {
            return this.mockLocalStorage[item];
        } catch (ex) {
            throw new Error('Local storage getItem failed because item does not exist');
        }
      }
    }

    public removeItem(item: string) {
        try {
            if (!GlobalFunctions.isNullOrUndefined(localStorage)) {
                return localStorage.removeItem(item);
            } else {
                this.mockLocalStorage[item] = null;
            }
        } catch (ex) {
          throw new Error('Local storage item does not exist');
        }
    }
}
