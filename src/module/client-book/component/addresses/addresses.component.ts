﻿import { Component, OnInit } from '@angular/core';
import { Address } from '../../model/address.class';
import { AddressSmartComponent } from './address-smart/address-smart.component';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { GridHeaderModel } from '@global/component/grid/grid-header.model';
import { of } from 'rxjs/observable/of';
import { GenericsComponent } from '@global/component/generic/generics.component';
import { GenericService } from '@global/service/generic.service';
import { ModelBase } from '@global/model/model-base.class';

@Component({
    selector: 'app-addresses',
    templateUrl: './addresses.component.html'
})

export class AddressesComponent extends GenericsComponent<Address, AddressSmartComponent> implements OnInit {  public isBusy: boolean;
  public addressHeaders: GridHeaderModel[];
  public headerTranslation: {};
  constructor(public dialogService: DialogService, public addressService: GenericService) {
    super(new Address(), dialogService, addressService);
    this.addressHeaders =  [
      GridHeaderModel.CreateTextHeader('houseNo', 'houseNo', true),
      GridHeaderModel.CreateTextHeader('postCode', 'postCode', true),
      GridHeaderModel.CreateTextHeader('cityTown', 'cityTown.name', true),
      GridHeaderModel.CreateTextHeader('county', 'cityTown.county.name', true),
      GridHeaderModel.CreateTextHeader('region', 'cityTown.county.region.name', true),
      GridHeaderModel.CreateDeleteHeader('id', 'id', false),
    ];
    // Mocked translations
    this.headerTranslation = {
      'houseNo': 'House No',
      'postCode': 'Postcode',
      'cityTown': 'Town',
      'county': 'County',
      'region': 'Region',
      'id': ''
    };
    this.isBusy = true;
  }

  ngOnInit() {
    this.isBusy = true;
    this.addressService.getAll(new Address()).subscribe(x => {
      this.data = of(x);
      this.isBusy = false;
    });
  }

  public openCreate = () => {
      this.showDetails(this.entity, this.dialogService.addDialog(AddressSmartComponent, {
        title: 'Address',
        entity: this.entity
    }) );
  }

  public show(address: ModelBase) {
      console.log('');
      console.log('');
      console.log('');
      this.showDetails(address, this.dialogService.addDialog(AddressSmartComponent, {
        title: 'Address',
        entity: address
      }) );
  }
}
