﻿import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Address } from '../../../model/address.class';
import { CityTown } from '../../../model/city-town.class';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { GlobalFunctions } from '../../../../Global/global-function.constant';
import { Observable } from 'rxjs/Observable';
import { tap, catchError } from 'rxjs/operators';
import { ModelBase } from '@global/model/model-base.class';
import { GenericComponent } from '@global/component/generic/generic.component';
import { GenericService } from '@global/service/generic.service';

export interface AddressModel {
    title: string;
    entity: ModelBase;
    saveSucceeded: EventEmitter<string>;
}
@Component({
    selector: 'app-address-smart',
    templateUrl: './address-smart.component.html'
})


export class AddressSmartComponent extends GenericComponent<AddressModel> implements AddressModel, OnInit {
    complexForm: FormGroup;
    addressModel: Address;
    saveSucceeded: EventEmitter<string>;

    public cityTowns: CityTown[];
    public loaded: boolean;
    public title: string;

    constructor(dialogService: DialogService, fb: FormBuilder,
        public addressService: GenericService) {
      super(dialogService, addressService);

        this.isBusy = true;
        this.complexForm = fb.group({
            'id': [null],
            'house-no': [null, Validators.required],
            'street': [null, Validators.required],
            'town-city': [null, Validators.required],
            'area': [null],
            'post-code': [null, Validators.required],
        });
    }

    ngOnInit() {
      this.isBusy = true;
      const city = new CityTown('');
      this.genericService.getAll(city).subscribe(result => {
        this.cityTowns =  result;
        this.incrementProcess();
      });
    }
}
