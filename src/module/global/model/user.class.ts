﻿import { Permission } from './permission.class';

export class User {
    public id: number;
    public username: string;
    public permissions: Permission[];
}
