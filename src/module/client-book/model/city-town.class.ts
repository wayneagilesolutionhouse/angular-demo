﻿import { County } from './county.class';
import { Region } from './region.class';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GlobalFunctions } from '../../Global/global-function.constant';
import { ModelBase } from '../../Global/model/model-base.class';

export class CityTown extends ModelBase {
  public county: County;

  constructor(town: string) {
    super();
    this.name = town;
    this.path = 'ClientBook/Towns';
  }

  static instantiate = (id: number, name: string, county: County) => {
    const returnValue = new CityTown(name);
    returnValue.id = id;
    returnValue.county = county;
    return returnValue;
  }

  static mapModelToServiceModel(model: CityTown): any {
    const entity = new CityTown('');
    return entity.mapModelToServiceModel(model);
  }

  static mapServiceModelToModel(serviceModel: any): CityTown {
    const entity = new CityTown('');
    return entity.mapServiceModelToModel(serviceModel);
  }

  static mapServiceModelsToModels(servicesModel: any) {
    const entity = new CityTown('');
    return entity.mapServiceModelsToModels(servicesModel);
  }

  static mapFormToModel = (complexForm: FormGroup, id: number): CityTown => {
    const name = !GlobalFunctions.isNullOrUndefined(
      complexForm.get('name').value
    )
      ? complexForm.get('name').value
      : null;
    const county = !GlobalFunctions.isNullOrUndefined(
      complexForm.get('county').value
    )
      ? complexForm.get('county').value
      : null;
    return CityTown.instantiate(id, name, county);
  }
  public mapModelToServiceModel(model: CityTown): any {
    return GlobalFunctions.isNullOrUndefined(model)
      ? null
      : {
          Name: model.name,
          ID: model.id,
          Active: !model.markForDeletion,
          CountyId: model.county.id
        };
  }

  public mapServiceModelToModel(serviceModel: any): CityTown {
    const cityTown = new CityTown(serviceModel.name);
    cityTown.id = serviceModel.id;
    cityTown.markForDeletion = !serviceModel.active;
    cityTown.county = !GlobalFunctions.isNullOrUndefined(serviceModel.county)
      ? serviceModel.county
      : null;
    return cityTown;
  }

  public mapServiceModelsToModels(servicesModel: any) {
    const returnValue: CityTown[] = [];

    if (Array.isArray(servicesModel)) {
      servicesModel.forEach(serviceModel => {
        returnValue.push(CityTown.mapServiceModelToModel(serviceModel));
      });
    }
    return returnValue;
  }
}
