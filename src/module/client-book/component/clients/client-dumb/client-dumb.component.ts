﻿import { Component, Input } from '@angular/core';
import { Region } from '../../../model/region.class';
import { Country } from '../../../model/country.class';
import { Client } from '../../../model/client.class';
import { CityTown } from '../../../model/city-town.class';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';

export interface ClientModel {
    title: string;
    client: Client;
}

@Component({
    selector: 'app-client-dumb',
    templateUrl: './client-dumb.component.html'
})

export class ClientDumbComponent {
    clientModel: Client;
    title: string;
    @Input() cityTowns: CityTown[];
    @Input() group: FormGroup;
    @Input() set client(c: Client) {
        this.clientModel = c;
    }
    constructor(fb: FormBuilder) {
    }
}
