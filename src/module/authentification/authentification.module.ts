import { NgModule, ModuleWithProviders } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { authenticationRouting } from './authentification.routing';
// Components
import { LoginComponent } from './component/login/login.component';
import { RegisterComponent } from './component/register/register.component';
import { AlertService } from './service/alert.service';
import { AuthenticationService } from './service/authentification.service';
import { LocalStorageService } from './service/local-storage.service';
import { AuthGuard } from './guard/auth.gaurd';
import { GlobalModule } from '../global/global.module';
import { LocalStorageModule } from 'angular-2-local-storage';
// Services
import { UserService } from './service/user.service';
// import { HttpInterceptionService } from './service/http-interception.service';
import { CustomHttp } from '../global/custom-http.service';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpInterceptionService } from './service/http-interception.service';



/** Http interceptor providers in outside-in order */
export const httpInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: HttpInterceptionService, multi: true },
];

@NgModule({
  imports: [authenticationRouting, FormsModule, ReactiveFormsModule, CommonModule, GlobalModule,
    LocalStorageModule.withConfig({
      prefix: 'my-app',
      storageType: 'localStorage'
  })],
  providers: [AlertService,
              AuthenticationService,
              LocalStorageService,
              UserService,
              CustomHttp,
              AuthGuard,
              httpInterceptorProviders],
  declarations: [LoginComponent, RegisterComponent]
})

export class AuthentificationModule {

}


