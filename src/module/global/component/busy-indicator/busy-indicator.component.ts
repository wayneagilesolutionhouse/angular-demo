import {
    AfterContentInit,
    Component,
    ContentChild,
    ElementRef,
    HostBinding,
    Input, OnDestroy
} from '@angular/core';

@Component({
    selector: 'app-busy-indicator',
    templateUrl: './busy-indicator.component.html',
    styleUrls: ['./busy-indicator.component.scss']
})
export class BusyIndicatorComponent  {
    @Input() busy: boolean;
    @Input() hexColor: string;
}
