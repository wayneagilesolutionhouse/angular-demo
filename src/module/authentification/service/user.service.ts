﻿import { Injectable } from '@angular/core';
import { ConnectionBackend, XHRBackend, RequestOptions, Request, Response, Http, Headers } from '@angular/http';
import { User } from '../model/user';
import { Observable } from 'rxjs/Observable';
import { CustomHttp } from '../../global/custom-http.service';
import { GlobalFunctions } from '../../global/global-function.constant';
import { Guid } from 'guid-typescript';

@Injectable()
export class UserService {
    static readonly COUNTRY_WEBAPI: string = 'api/CredentialBook';
    constructor(private http: Http, customHttp: CustomHttp) {
      this.customHttp = customHttp;
      this.users = [];
    }
    private customHttp: CustomHttp;
    private users: User[];
    public getAll() {
      return this.customHttp.get(UserService.COUNTRY_WEBAPI + '/' + 'Users').map(x => {
          return User.mapServiceModelsToModels(x.json());
      });
    }

    public getByUserNamePassword(username: string, password: string) {
      const myParams = new URLSearchParams();
      myParams.append('user', username);
      myParams.append('pwd', password);
      const options = new RequestOptions({  params: myParams });
      return this.customHttp.get(UserService.COUNTRY_WEBAPI + '/' + 'Account', options).map(x => {
          return User.mapServiceModelsToModels(x.json());
      });
    }

    public postMiscelaneous(entity: User, apiPath: string): Observable<User> {
      return this.customHttp.post(UserService.COUNTRY_WEBAPI  + '/' + apiPath,
       JSON.stringify(User.mapModelToServiceModel(entity))).map(x => {
        return x.json();
      });
    }

    public create(entity: User): Observable<User> {
      return this.customHttp.post(UserService.COUNTRY_WEBAPI  + '/' + 'Account',
       JSON.stringify(User.mapModelToServiceModel(entity))).map(x => {
        return User.mapServiceModelToModel(x.json());
      });
    }

    public update(user: User) {
      const userServiceModel = User.mapModelToServiceModel(user);
      const id = !GlobalFunctions.isNullOrUndefined(user._id) ? user._id : 0;
      userServiceModel.rememberMe = true;
      userServiceModel.token = Guid.raw;
      return this.customHttp.put(UserService.COUNTRY_WEBAPI + '/' + 'Account' + '/' + id ,
      JSON.stringify(userServiceModel), null)
      .map(x => {
        return userServiceModel;
    });
   }

    delete(user: User) {
      const id = !GlobalFunctions.isNullOrUndefined(user.token) ? user.token : 0;
      return this.customHttp.delete(UserService.COUNTRY_WEBAPI + '/' + 'Account' + '/' + id).map(x => {
        return User.mapServiceLoginModelToModel(x.json());
    });
    }
}
