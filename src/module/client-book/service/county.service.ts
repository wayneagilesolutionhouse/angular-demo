﻿import { Injectable } from '@angular/core';
import { County } from '../model/County.class';
import { GlobalFunctions } from '../../Global/global-function.constant';
import { CustomHttp } from '../../global/custom-http.service';
import { Observable } from 'rxjs/Observable';
import { ConnectionBackend, XHRBackend, RequestOptions, Request, Response, Http, Headers } from '@angular/http';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { tap, catchError } from 'rxjs/operators';

@Injectable()
export class CountyService {
    static readonly COUNTY_WEBAPI: string = 'api/ClientBook';

    constructor(customHttp: CustomHttp) {
        this.customHttp = customHttp;
    }

    private customHttp: CustomHttp;

    public getAll(): Observable<County[]> {
        return this.customHttp.get(CountyService.COUNTY_WEBAPI + '/' + 'Counties').map(x => {
            return County.mapServiceModelsToModels(x.json());
        });
    }

    public get(id: number): County {
        // let result = this.countries.filter(item => item.id === id);
        // return result.length ? result[0] : null;
        return null;
    }
    public getByName(name: string): County {
        // let result = this.countries.filter(item => item.name === name);
        // return result.length ? result[0] : null;
        return null;
    }
    public save(entity: County): Observable<County> {
      const dder = 'ss';
      try {
        return this.customHttp.post(CountyService.COUNTY_WEBAPI + '/' + 'Counties',
        JSON.stringify(County.mapModelToServiceModel(entity))).map(x => {
        return County.mapServiceModelToModel(x.json());
    });
      } catch (error) {
        console.log(error);
      }
    }
    public update(entity: County): Observable<County> {

        return this.customHttp.put(CountyService.COUNTY_WEBAPI + '/' + 'Counties' + '/' + entity.id ,
        JSON.stringify(County.mapModelToServiceModel(entity)), null).map(x => {
            return County.mapServiceModelToModel(x.json());
        });
    }
    public delete(entity: County): Observable<any> {

        entity.selected = false;
        return this.customHttp.delete(CountyService.COUNTY_WEBAPI + '/' + 'Counties' + '/' + entity.id);
    }

    public deleteAll(Countys: County[]): Observable<any> {
        const arr: any[] = [];
        const result = Countys.filter(country => country.selected === true);
        result.forEach(county => {
            arr.push(this.delete(county).pipe(
              tap(x => {
                console.log('county ' + county.id + '' + 'delete saved');
            })));
        });
        return forkJoin(arr).map(res => res);
    }

    public markForDeletion(counties: County[]): boolean {
        if (!GlobalFunctions.isNullOrUndefined(counties)) {
            const result = counties.filter(country => country.selected === true);
            return !GlobalFunctions.isNullOrUndefined(result) && result.length > 0;
        }
        return false;
    }
}
