import { NgModule, ModuleWithProviders } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClientBookRouting } from './client-book.routing';
import { GlobalModule } from '../global/global.module';
import { CommonModule } from '@angular/common';
// Table Components
import { ClientsComponent } from './component/clients/clients.component';
import { ContactsComponent } from './component/contacts/contacts.component';
import { AddressesComponent } from './component/addresses/addresses.component';
import { TownCitiesComponent } from './component/town-cities/town-cities.component';
import { CountiesComponent } from './component/counties/counties.component';
import { RegionsComponent } from './component/regions/regions.component';
import { CountriesComponent } from './component/countries/countries.component';
// Modal Components
import { CountryComponent } from './component/countries/country/country.component';
import { RegionComponent } from './component/regions/region/region.component';
import { CountyComponent } from './component/counties/county/county.component';
import { CityTownComponent } from './component/town-cities/town-city/town-city.component';
import { AddressDumbComponent } from './component/addresses/address-dumb/address-dumb.component';
import { AddressSmartComponent } from './component/addresses/address-smart/address-smart.component';
import { ContactSmartComponent } from './component/contacts/contact-smart/contact-smart.component';
import { ContactDumbComponent } from './component/contacts/contact-dumb/contact-dumb.component';
import { ClientSmartComponent } from './component/clients/client-smart/client-smart.component';
import { ClientDumbComponent } from './component/clients/client-dumb/client-dumb.component';
// Other components
import { BootstrapModalModule } from 'ng2-bootstrap-modal';

// Services
import { CountryService } from './service/country-service';
import { CountyService } from './service/county.service';
import { RegionService } from './service/region.service';
import { TownService } from './service/town.service';
import { GenericService } from '../../module/global/service/generic.service';
import { DialogService } from 'ng2-bootstrap-modal';

@NgModule({
  imports: [ClientBookRouting, FormsModule, BootstrapModalModule,
    ReactiveFormsModule,  CommonModule, GlobalModule],
  declarations: [ ClientsComponent,
    ContactsComponent,
    AddressesComponent,
    TownCitiesComponent,
    CountiesComponent,
    RegionsComponent,
    CountriesComponent,
    CountryComponent,
    RegionComponent,
    CountyComponent,
    CityTownComponent,
    AddressSmartComponent,
    AddressDumbComponent,
    ContactDumbComponent,
    ContactSmartComponent,
    ClientSmartComponent,
    ClientDumbComponent
    ],
  exports: [
    ClientSmartComponent,
    ContactsComponent,
    AddressesComponent,
    TownCitiesComponent,
    CountiesComponent,
    RegionsComponent,
    CountriesComponent,
    CountryComponent,
    RegionComponent,
    CountyComponent,
    CityTownComponent,
    AddressSmartComponent,
    AddressDumbComponent,
    ContactDumbComponent,
    ContactSmartComponent,
    ClientSmartComponent,
    ClientDumbComponent
  ],
  entryComponents: [
    ClientsComponent,
    CountryComponent,
    RegionComponent,
    CountyComponent,
    CityTownComponent,
    AddressSmartComponent,
    AddressDumbComponent,
    ContactDumbComponent,
    ContactSmartComponent,
    ClientSmartComponent,
    ClientDumbComponent],
  providers: [ DialogService, CountryService, CountyService, GenericService,
    RegionService, TownService, DialogService]
})

export class ClientBookModule {
}
