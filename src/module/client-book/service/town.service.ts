﻿import { Injectable } from '@angular/core';
import { CityTown } from '../model/city-town.class';
import { GlobalFunctions } from '../../Global/global-function.constant';
import { CustomHttp } from '../../global/custom-http.service';
import { Observable } from 'rxjs/Observable';
import { ConnectionBackend, XHRBackend, RequestOptions, Request, Response, Http, Headers } from "@angular/http";
import { forkJoin } from 'rxjs/observable/forkJoin';
import { tap, catchError } from 'rxjs/operators';

@Injectable()
export class TownService {
    static readonly COUNTRY_WEBAPI: string = 'api/ClientBook';

    constructor(customHttp: CustomHttp) {
        this.customHttp = customHttp;
    }

    private customHttp: CustomHttp;

    public getAll(): Observable<CityTown[]> {
        return this.customHttp.get(TownService.COUNTRY_WEBAPI + '/' + 'towns').map(x => {
            return CityTown.mapServiceModelsToModels(x.json());
        });
    }

    public get(id: number): CityTown {
        // let result = this.countries.filter(item => item.id === id);
        // return result.length ? result[0] : null;
        return null;
    }
    public getByName(name: string): CityTown {
        // let result = this.countries.filter(item => item.name === name);
        // return result.length ? result[0] : null;
        return null;
    }
    public save(entity: CityTown): Observable<CityTown> {
      console.log('');
      console.log('');
      console.log('');
        return this.customHttp.post(TownService.COUNTRY_WEBAPI + '/' + 'towns',
            JSON.stringify(CityTown.mapModelToServiceModel(entity))).map(x => {
            return CityTown.mapServiceModelToModel(x.json());
        });
    }
    public update(entity: CityTown): Observable<CityTown> {
        return this.customHttp.put(TownService.COUNTRY_WEBAPI + '/' + 'towns' + '/' + entity.id ,
        JSON.stringify(CityTown.mapModelToServiceModel(entity)), null).map(x => {
            return CityTown.mapServiceModelToModel(x.json());
        });
    }
    public delete(entity: CityTown): Observable<any> {
        entity.selected = false;
        return this.customHttp.delete(TownService.COUNTRY_WEBAPI + '/' + 'towns' + '/' + entity.id);
    }

    public deleteAll(towns: CityTown[]): Observable<any> {
        const arr: any[] = [];
        const result = towns.filter(town => town.selected === true);
        result.forEach(town => {
            arr.push(this.delete(town).pipe(
              tap(x => {
                console.log('town ' + town.id + '' + 'delete saved');
            })));
        });
        return forkJoin(arr).map(res => res);
    }

    public markForDeletion(towns: CityTown[]): boolean {
        if (!GlobalFunctions.isNullOrUndefined(towns)) {
            const result = towns.filter(town => town.selected === true);
            return !GlobalFunctions.isNullOrUndefined(result) && result.length > 0;
        }
        return false;
    }
}
