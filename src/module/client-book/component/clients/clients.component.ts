﻿import { Component, OnInit } from '@angular/core';
import { Client } from '../../model/client.class';
import { ClientSmartComponent } from './client-smart/client-smart.component';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { Observable } from 'rxjs/Observable';
import { GridHeaderModel } from '@global/component/grid/grid-header.model';
import { of } from 'rxjs/observable/of';
import { GenericsComponent } from '@global/component/generic/generics.component';
import { GenericService } from '@global/service/generic.service';
import { ModelBase } from '@global/model/model-base.class';

@Component({
    selector: 'app-clients',
    templateUrl: './clients.component.html'
})

export class ClientsComponent extends GenericsComponent<Client, ClientSmartComponent> implements OnInit {
  public isBusy: boolean;
  public clientHeaders: GridHeaderModel[];
  public headerTranslation: {};
  constructor(public dialogService: DialogService, public contactService: GenericService) {
    super(Client.instantiate(), dialogService, contactService);
    this.clientHeaders =  [
      GridHeaderModel.CreateTextHeader('firstname', 'contact.firstname', true),
      GridHeaderModel.CreateTextHeader('surname', 'contact.surname', true),
      GridHeaderModel.CreateTextHeader('telephone', 'contact.telephone', true),
      GridHeaderModel.CreateTextHeader('email', 'contact.email', true),
      GridHeaderModel.CreateTextHeader('town', 'address.cityTown.name', true),
      GridHeaderModel.CreateDeleteHeader('id', 'id', false),
    ];
    // Mocked translations
    this.headerTranslation = {
      'firstname': 'Firstname',
      'surname': 'Surname',
      'telephone': 'Telephone',
      'email': 'Email',
      'town': 'Town',
      'id': ''
    };
    this.isBusy = true;
  }

  ngOnInit() {
    this.isBusy = true;
    this.contactService.getAll(Client.instantiate()).subscribe(x => {
      this.data = of(x);
      this.isBusy = false;
    });
  }

  public openCreate = () => {
      this.showDetails(this.entity, this.dialogService.addDialog(ClientSmartComponent, {
        title: 'Client',
        entity: this.entity
    }) );
  }

  public show(client: ModelBase) {
      this.showDetails(client, this.dialogService.addDialog(ClientSmartComponent, {
        title: 'Client',
        entity: client
      }) );
  }
}
