export class GlobalFunctions {
    public static valid = (value) => {
        return GlobalFunctions.isNullOrUndefined(value) ? value : '';
    }

    public static isUndefined = (item) => {
        if (typeof item === 'undefined') {
            return true;
        }
        if (item === null) {
            return true;
        }

        return false;
    }

    public static isNullOrUndefined = (item) => {
        if (typeof item === 'undefined') {
            return true;
        }
        if (item === null) {
            return true;
        }

        return false;
    }

    public static isEmptyObject(obj) {
        for (const prop in obj) {
            if (obj.hasOwnProperty(prop)) {
                return false;
            }
        }

        return true && JSON.stringify(obj) === JSON.stringify({});
    }

     public static isNullOrUndefinedOrEmptyObject = (obj) => {
        if (GlobalFunctions.isNullOrUndefined(obj)) {
            return true;
        }

        if (GlobalFunctions.isEmptyObject(obj)) {
            return true;
        }

        return false;
    }
}
