export class GridHeaderModel {

    public dateFormat: string;
    constructor (
        public columnTranslationKey: string,
        public propertyName: string,
        public linkName: string,
        public isDate: boolean,
        public type: string = 'string',
        public pipe: string = '',
        public sort: boolean = true
    ) {}

    static CreateDateHeader(columnTranslationKey: string, propertyName: string, format: string, sortable: boolean): GridHeaderModel {
        const returnValue = new GridHeaderModel(columnTranslationKey, propertyName, '', true, '', '', sortable);
        returnValue.dateFormat = format;
        return returnValue;
    }

    static CreateLinkHeader(columnTranslationKey: string, propertyName: string, linkName: string,
                            type: string, sortable: boolean): GridHeaderModel  {
        return  new GridHeaderModel(columnTranslationKey, propertyName, linkName, false, type);
    }

    static CreateNumberHeader(columnTranslationKey: string, propertyName: string, sortable: boolean): GridHeaderModel {
        return new GridHeaderModel(columnTranslationKey, propertyName, '', false, 'integer', '', sortable);
    }

    static CreateTextHeader(columnTranslationKey: string, propertyName: string, sortable: boolean) {
        return new GridHeaderModel(columnTranslationKey, propertyName, '', false, 'string', '', sortable);
    }

    static CreateOneToManyHeader(columnTranslationKey: string, propertyName: string) {
        return new GridHeaderModel(columnTranslationKey, propertyName, '', false, 'OneToMany', '');
    }

    static CreateDeleteHeader(columnTranslationKey: string, propertyName: string, sortable: boolean) {
      return new GridHeaderModel(columnTranslationKey, propertyName, '', false, 'action', '', sortable);
    }
}
