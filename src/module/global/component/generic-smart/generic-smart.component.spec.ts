import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenericSmartComponent } from './generic-smart.component';

describe('GenericSmartComponent', () => {
  let component: GenericSmartComponent;
  let fixture: ComponentFixture<GenericSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenericSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenericSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
