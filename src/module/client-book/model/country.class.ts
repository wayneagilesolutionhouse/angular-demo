﻿
import { GlobalFunctions } from '../../Global/global-function.constant';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ModelBase } from '../../Global/model/model-base.class';

export class Country extends ModelBase {
  id: number;
  selected: boolean;
  path: string;

    constructor(country: string, id: number) {
        super();
        this.name = country;
        this.id = id;
        this.selected = false;
        this.path = 'ClientBook/Countries';
    }

    static instantiate() {
      return new Country('', 0);
    }

    static mapModelToServiceModel(model: Country): any {
      const entity = Country.instantiate();
      return entity.mapModelToServiceModel(model);
    }

    static mapServiceModelToModel(serviceModel: any): Country {
      const entity = Country.instantiate();
      return entity.mapServiceModelToModel(serviceModel);
    }

    static mapServiceModelsToModels(servicesModel: any) {
        const entity = Country.instantiate();
        return entity.mapServiceModelsToModels(servicesModel);
    }

    static mapFormToModel = (complexForm: FormGroup, id: number): Country => {
        const name = !GlobalFunctions.isNullOrUndefined(complexForm.get('name').value) ? complexForm.get('name').value : null;
        const country = new Country(name, id);
        country.markForDeletion = !GlobalFunctions.isNullOrUndefined(complexForm.get('markForDeletion').value) ?
        complexForm.get('markForDeletion').value : false;
        return new Country(name, id);
    }

    public mapModelToServiceModel(model: Country): any {
      return GlobalFunctions.isNullOrUndefined(model) ? null : {
          Name: model.name,
          ID: model.id,
          Active: !model.markForDeletion
      };
    }

    public mapServiceModelToModel(serviceModel: any): Country {
        const country = new Country(serviceModel.name, serviceModel.id);
        country.markForDeletion = !serviceModel.active;
        return country;
    }

    public mapServiceModelsToModels(servicesModel: any) {
        const returnValue: Country[] = [];

        if (Array.isArray(servicesModel)) {
            servicesModel.forEach(serviceModel => {
                returnValue.push(Country.mapServiceModelToModel(serviceModel));
            });
        }
        return returnValue;
    }
}
