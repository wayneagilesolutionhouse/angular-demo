﻿
import { Region } from './region.class';
import { GlobalFunctions } from '../../Global/global-function.constant';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ModelBase } from '../../Global/model/model-base.class';

export class County extends ModelBase {
    constructor(county: string) {
        super();
        this.name = county;
        this.path = 'ClientBook/counties';
    }
    public region: Region;
    static instantiate(id: number, name: string, region: Region) {
        const returnValue = new County(name);
        returnValue.id = id;
        returnValue.region = region;
        return returnValue;
    }

    static mapModelToServiceModel(model: County): any {
      return GlobalFunctions.isNullOrUndefined(model) ? null : {
          Name: model.name,
          ID: model.id,
          Active: !model.markForDeletion,
          RegionId: !GlobalFunctions.isNullOrUndefined(model.region) ? model.region.id : 0
      };
    }

    static mapServiceModelToModel(serviceModel: any): County {
        const county = new County(serviceModel.name);
        county.id = serviceModel.id;
        county.markForDeletion = !serviceModel.active;
        county.region = Region.mapServiceModelToModel(serviceModel.region);
        return county;
    }

  static mapServiceModelsToModels(servicesModel: any) {
    const returnValue: County[] = [];

    if (Array.isArray(servicesModel)) {
        servicesModel.forEach(serviceModel => {
            returnValue.push(County.mapServiceModelToModel(serviceModel));
        });
    }
    return returnValue;
  }

    static mapFormToModel = (complexForm: FormGroup, id: number): County => {
        const name = !GlobalFunctions.isNullOrUndefined(complexForm.get('name').value) ? complexForm.get('name').value : null;
        const region = !GlobalFunctions.isNullOrUndefined(complexForm.get('region').value) ? complexForm.get('region').value : null;
        return County.instantiate(id, name, region);
    }
}
