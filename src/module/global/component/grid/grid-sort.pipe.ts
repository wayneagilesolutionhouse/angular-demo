import { Pipe, PipeTransform } from '@angular/core';
import { GridSort } from '../grid/grid-sort.model';
import * as _ from 'lodash';
import { ScalarObservable } from 'rxjs/observable/ScalarObservable';
import { GlobalFunctions } from '../../global-function.constant';

@Pipe({
    name: 'gridSort'
})
export class GridSortPipe implements PipeTransform {
    transform(array: any, sort: string): ScalarObservable<any> {
        if (GlobalFunctions.isNullOrUndefined(sort)) {
                return array;
        }

        // Deserialise array parameter to gridsort object
        const arr = sort.split('-');
        const gridSort = new GridSort(arr[0], arr[1]);

        if (arr[2] === 'integer') {
            array.value =
                gridSort.sortOrder === 'asc'
                    ? array.value.sort(function(a, b) {
                          return a[gridSort.key] - b[gridSort.key];
                      })
                    : array.value.sort(function(a, b) {
                          return b[gridSort.key] - a[gridSort.key];
                      });
        } else if (arr[2] === 'date' ) {
            array.value =
            gridSort.sortOrder === 'asc'
                ? array.value.sort(function(a, b) {
                    const c: any = new Date(a.date);
                    const d: any = new Date(b.date);
                    return c - d;
                  })
                : array.value.sort(function(a, b) {
                    const c: any = new Date(a.date);
                    const d: any = new Date(b.date);
                    return d - c;
                  });
        // This will do string sort for undifined types
        } else {
            array.value =
                gridSort.sortOrder === 'asc'
                    ? array.value.sort(function(a, b) {
                        if (a[gridSort.key] < b[gridSort.key]) {
                            return -1;
                        } else {
                            return 0;
                        }
                    }) :  array.value.sort(function(a, b) {
                        if (a[gridSort.key] > b[gridSort.key]) {
                            return -1;
                        } else {
                            return 0;
                        }
                    });
        }

        return array;
    }
}
