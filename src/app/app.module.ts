import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthentificationModule } from '../module/authentification/authentification.module';
import { GlobalModule } from '../module/global/global.module';
import { authenticationRouting } from '../module/authentification/authentification.routing';
import { ClientBookModule } from '../module/client-book/client-book.module';
import { ClientBookRouting } from '../module/client-book/client-book.routing';
// import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { LoadingModule } from 'ngx-loading';
import { NgxService } from '../module/global/service/ngx.service';
import { AuthGuard } from '../module/authentification/guard/auth.gaurd';
// Components
import { AppComponent } from './app.component';
import { NavMenuComponent } from '../component/navmenu/navmenu.component';
import { HomeComponent } from '../component/home/home.component';

@NgModule({
  declarations: [AppComponent, NavMenuComponent, HomeComponent],
  imports: [
    HttpModule,
    BrowserModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    AuthentificationModule,
    ClientBookModule,
    GlobalModule,
    authenticationRouting,
   // ClientBookRouting,
   // EccommerceBookRouting,
    RouterModule.forRoot([
      {
        path: 'authentification',
        loadChildren:
          'module/authentification/authentification.module#AuthentificationModule',
        canActivateChild: [AuthGuard]
      },
      {
        path: 'client-book',
        loadChildren: 'module/client-book/client-book.module#ClientBookModule',
        canActivateChild: [AuthGuard]
      },
      // {
      //   path: 'eccommerce',
      //   loadChildren: 'module/eccommerce/eccommerce-book.module#EccommerceBookModule',
      //   canActivateChild: [AuthGuard]
      // },
      { path: 'Home', component: HomeComponent, canActivate: [AuthGuard] },
      { path: '**', redirectTo: 'Home', pathMatch: 'full' }
    ]),
    LoadingModule
    // Ng4LoadingSpinnerModule.forRoot()
  ],
  providers: [NgxService],
  bootstrap: [AppComponent]
})
export class AppModule {}
