import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { GenericService } from '../../service/generic.service';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

export class GenericsComponent<T, TSmartComponent> implements OnInit {
  @Input() isManaged = false;
  @Output() openProperties: EventEmitter<any> = new EventEmitter<any>();
  public page: Number;
  @Input() data: Observable<any[]>;
  @Output() dataEmitter: EventEmitter<Observable<any[]>>;
  public isBusy: boolean;

  constructor(
    public entity: any,
    public dialogService: DialogService,
    public genericService: GenericService
  ) {
    this.dataEmitter = new EventEmitter<Observable<any[]>>();
    this.page = 1;
  }

  ngOnInit() {
    this.getAll();
  }

  public getAll() {
    this.isBusy = true;
    this.genericService.getAll(this.entity).subscribe(x => {
      this.data = of(x);
      this.isBusy = false;
      this.dataEmitter.emit(this.data);
    });
  }

  public showDetails<T1>(selected: any, dialog: Observable<T1>) {
    if (!this.isManaged) {
      const self = this;
      const disposable = dialog.subscribe(isConfirmed => {
        // We get dialog result
        if (isConfirmed) {
          this.genericService.getAll(this.entity).subscribe(x => {
            this.data = of(x);
          });
        }
      });
    } else {
      this.openProperties.emit(selected);
    }
  }

  public deleteAll = (data: Observable<any[]>) => {
    const self = this;
    data.subscribe(dataItem => {
      self.genericService.deleteAll(dataItem).subscribe(x => {
        this.genericService.getAll(this.entity).subscribe(
          result => {
            this.data = of(result);
          },
          e => {
            console.log(e);
          }
        );
      });
    });
  }
}
