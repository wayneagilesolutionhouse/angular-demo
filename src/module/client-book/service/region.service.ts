﻿import { Injectable } from '@angular/core';
import { Region } from '../model/region.class';
import { GlobalFunctions } from '../../Global/global-function.constant';
import { CustomHttp } from '../../global/custom-http.service';
import { Observable } from 'rxjs/Observable';
import { ConnectionBackend, XHRBackend, RequestOptions, Request, Response, Http, Headers } from '@angular/http';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { tap, catchError } from 'rxjs/operators';

@Injectable()
export class RegionService {
    static readonly COUNTRY_WEBAPI: string = 'api/ClientBook';

    constructor(customHttp: CustomHttp) {
        this.customHttp = customHttp;
    }

    private customHttp: CustomHttp;

    public getAll(): Observable<Region[]> {
        return this.customHttp.get(RegionService.COUNTRY_WEBAPI + '/' + 'regions').map(x => {
            return Region.mapServiceModelsToModels(x.json());
        });
    }

    public get(id: number): Observable<Region[]> {
      return this.customHttp.get(RegionService.COUNTRY_WEBAPI + '/' + 'regions/' + id.toString()).map(x => {
        return Region.mapServiceModelsToModels(x.json());
      });
    }
    public getByName(name: string): Region {
        // let result = this.countries.filter(item => item.name === name);
        // return result.length ? result[0] : null;
        return null;
    }
    public save(entity: Region): Observable<Region> {
      console.log('test');
      console.log('test');
      console.log('test');
        return this.customHttp.post(RegionService.COUNTRY_WEBAPI + '/' + 'regions',
            JSON.stringify(Region.mapModelToServiceModel(entity))).map(x => {
            return Region.mapServiceModelToModel(x.json());
        });
    }
    public update(entity: Region): Observable<Region> {
        return this.customHttp.put(RegionService.COUNTRY_WEBAPI + '/' + 'regions' + '/' + entity.id ,
        JSON.stringify(Region.mapModelToServiceModel(entity)), null).map(x => {
            return Region.mapServiceModelToModel(x.json());
        });
    }
    public delete(entity: Region): Observable<any> {
        entity.selected = false;
        return this.customHttp.delete(RegionService.COUNTRY_WEBAPI + '/' + 'regions' + '/' + entity.id);
    }

    public deleteAll(regions: Region[]): Observable<any> {
        const arr: any[] = [];
        const result = regions.filter(country => country.selected === true);
        result.forEach(region => {
            arr.push(this.delete(region).pipe(
              tap(x => {
                console.log('country ' + region.id + '' + 'delete saved');
            })));
        });
        return forkJoin(arr).map(res => res);
    }

    public markForDeletion(countries: Region[]): boolean {
        if (!GlobalFunctions.isNullOrUndefined(countries)) {
            const result = countries.filter(country => country.selected === true);
            return !GlobalFunctions.isNullOrUndefined(result) && result.length > 0;
        }
        return false;
    }
}
