﻿import { Component, Input, OnInit } from '@angular/core';
import { Region } from '../../../model/region.class';
import { Country } from '../../../model/country.class';
import { County } from '../../../model/county.class';
import { CityTown } from '../../../model/city-town.class';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { CountryService } from '../../../service/country-service';
import { RegionService } from '../../../service/region.service';
import { CountyService } from '../../../service/county.service';
import { TownService } from '../../../service/town.service';
import { GlobalFunctions } from '../../../../Global/global-function.constant';
import { Observable } from 'rxjs/Observable';
import { isNullOrUndefined } from 'util';
// import { Select2OptionData } from 'ng2-select2';

export interface CityTownModel {
    title: string;
    cityTown: CityTown;
}

@Component({
    selector: 'app-town-city',
    templateUrl: './town-city.component.html'
})

export class CityTownComponent extends DialogComponent<CityTownModel,
 boolean> implements CityTownModel, OnInit {
  complexForm: FormGroup;
  _cityTown: CityTown;
  title: string;

  public counties: County[];
  public countries: Country[];
  public regions: Region[];
  public _cityRegion: CityTown;

  constructor(dialogService: DialogService, fb: FormBuilder, private countryService: CountryService,
        private regionService: RegionService, private countyService: CountyService, private townService: TownService) {
        super(dialogService);
        this.complexForm = fb.group({
           'name': [null, Validators.required],
            'county': [null, Validators.required],
            'region': [null, Validators.required],
            'country': [null, Validators.required]
        });
  }

  public compareFn(option, selected): boolean {
    return !isNullOrUndefined(option) && !isNullOrUndefined(selected) ?
      option.id === selected.id : false;
  }

  // private assignDropdown<T>(src: Observable<T[]>, controlName: string, comparePropertyName: string, compareValue: string) {
  //   src.subscribe(data => data.forEach(x => {
  //     if (!GlobalFunctions.isNullOrUndefined(x) && !GlobalFunctions.isNullOrUndefined(x)) {
  //       if (x[comparePropertyName] === compareValue) {
  //           this.complexForm.controls[controlName].setValue(x);
  //       }
  //     }
  //   }));
  // }

  ngOnInit() {
    // const county = !isNullOrUndefined(this._cityTown.county) ? this._cityTown.county : null;
    // const region = !isNullOrUndefined(this._cityTown.county) &&
    //                !isNullOrUndefined(this._cityTown.county.region) ? this._cityTown.county.region : null;
    // const country = !isNullOrUndefined(this._cityTown.county) &&
    //                !isNullOrUndefined(this._cityTown.county.region) ? this._cityTown.county.region.country : null;
    this.countryService.getAll().subscribe(x => {
      this.countries = x;
    });
    // this.assignDropdown(this.countries, 'country', 'name', country !== null ? country.name : '');
    this.regionService.getAll().subscribe(x => {
      this.regions = x;
    });
    // this.assignDropdown(this.regions, 'region', 'name', region !== null ? country.name : '');
    this.countyService.getAll().subscribe(x => {
      this.counties = x;
    });
    // this.assignDropdown(this.counties, 'county', 'name', county !== null ? country.name : '');
  }

  get cityTown(): CityTown {
      return this._cityTown;
  }
  set cityTown(town: CityTown) {
    if (!isNullOrUndefined(town)) {
      this._cityTown = town;
      this.complexForm.controls['name'].setValue(town.name);
      this.complexForm.controls['county'].setValue(town.county);
      this.complexForm.controls['region'].setValue(town.county.region);
      this.complexForm.controls['country'].setValue(town.county.region.country);
    }
  }

  public switchMarkForDeletion() {
    this._cityTown.markForDeletion = !this._cityTown.markForDeletion;
  }

  public submitForm(value) {
      this._cityTown = CityTown.mapFormToModel(this.complexForm, this._cityTown.id);
      const outcome = this._cityTown.id > 0 ? this.townService.update(this._cityTown).subscribe(x => {
        if (!GlobalFunctions.isNullOrUndefined(x)) {
            this.result = true;
            this.close();
        }
    }) : this.townService.save(this._cityTown).subscribe(x => {
        if (!GlobalFunctions.isNullOrUndefined(x)) {
            this.result = true;
            this.close();
        }
    });
    this.result = false;
  }
}

