import { TestBed, inject } from '@angular/core/testing';

import { NgxService } from './ngx.service';

describe('NgxServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NgxService]
    });
  });

  it('should be created', inject([NgxService], (service: NgxService) => {
    expect(service).toBeTruthy();
  }));
});
