import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormGroup, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-generic-smart',
  templateUrl: './generic-smart.component.html',
  styleUrls: ['./generic-smart.component.scss']
})
export class GenericSmartComponent implements OnInit {

  constructor() {
    this.submitEmmiter = new EventEmitter<AbstractControl>();
    this.closeEmmiter = new EventEmitter<AbstractControl>();
   }
  @Input() isBusy: Boolean;
  @Input() heading: String;
  @Input() complexForm: FormGroup;
  @Output() submitEmmiter: EventEmitter<AbstractControl>;
  @Output() closeEmmiter: EventEmitter<AbstractControl>;
  ngOnInit() {
  }

  public debug() {
    const st = 1;
  }
}
