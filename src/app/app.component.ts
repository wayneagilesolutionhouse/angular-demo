import { Component, HostBinding } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxService } from '../module/global/service/ngx.service';

@Component({
    selector: 'body',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {

    constructor(private router: Router, private ngxService: NgxService) {
        this.routeValue = router;
    }

    @HostBinding('class.auth-body')
    public get isLogon(): boolean {
        return this.router.url.indexOf('/login') > -1 || this.router.url.indexOf('/register') > -1;
    }

    public get loading(): boolean {
      return this.ngxService.loading;
    }
    public routeValue: Router;
}
