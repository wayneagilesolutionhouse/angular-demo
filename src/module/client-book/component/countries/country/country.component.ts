﻿import { Component, Input } from '@angular/core';
import { Region } from '../../../model/region.class';
import { Country } from '../../../model/country.class';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { CountryService } from '../../../service/country-service';
import { GlobalFunctions } from '../../../../Global/global-function.constant';
import { isNullOrUndefined } from 'util';

export interface ConfirmModel {
    title: string;
    country: Country;
}
@Component({
    selector: 'app-country',
    templateUrl: './country.component.html'
})

export class CountryComponent extends DialogComponent<ConfirmModel, boolean> implements ConfirmModel {
    complexForm: FormGroup;
    _country: Country;
    title: string;

    get country(): Country {

        return this._country;
    }
    set country(c: Country) {
        this._country = c;
        this.complexForm.controls['name'].setValue(c.name);
        this.complexForm.controls['markForDeletion'].setValue(c.markForDeletion);
    }
    constructor(dialogService: DialogService, fb: FormBuilder, private countryService: CountryService) {
        super(dialogService);

        this.complexForm = fb.group({
            'name': [null, Validators.required],
            'markForDeletion': [null]
        });
    }

    public switchMarkForDeletion() {
      this.country.markForDeletion = !this.country.markForDeletion;
    }

    public submitForm(value) {
        this._country = Country.mapFormToModel(this.complexForm, this._country.id);
        const outcome = this._country.id > 0 ? this.countryService.update(this._country).subscribe(x => {
            if (!GlobalFunctions.isNullOrUndefined(x)) {
                this.result = true;
                this.close();
            }
        }) : this.countryService.save(this._country).subscribe(x => {
            if (!GlobalFunctions.isNullOrUndefined(x)) {
                this.result = true;
                this.close();
            }
        });
        this.result = false;
    }
}
