﻿import { Component, Input, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { Region } from '../../../model/region.class';
import { Country } from '../../../model/country.class';
import { CityTown } from '../../../model/city-town.class';
import { Address } from '../../../model/address.class';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TownService } from '../../../service/town.service';
import { GlobalFunctions } from '../../../../Global/global-function.constant';
import { Observable } from 'rxjs/Observable';
import { isNullOrUndefined } from 'util';

@Component({
    selector: 'app-address-dumb',
    templateUrl: './address-dumb.component.html'
})

export class AddressDumbComponent implements OnChanges {
    @Input() group: FormGroup;
    private entity: Address;

    @Input() set address(a: Address) {
        this.entity = a;
        if (!GlobalFunctions.isNullOrUndefined(a)) {
          this.group.controls['id'].setValue(a.id);
          this.group.controls['house-no'].setValue(a.houseNo);
          this.group.controls['street'].setValue(a.street);
          this.group.controls['area'].setValue(a.area);
          this.group.controls['post-code'].setValue(a.postCode);
          this.group.controls['town-city'].setValue(a.cityTown);
        }
    }

    @Input() cityTowns: CityTown[];
    constructor(fb: FormBuilder) {
    }

    ngOnChanges(changes: SimpleChanges) {
      if (changes['address']) {
        if (!GlobalFunctions.isNullOrUndefined(this.cityTowns) &&
            !GlobalFunctions.isNullOrUndefined(this.entity)) {
          this.cityTowns.forEach(townCity => {
              if (!GlobalFunctions.isNullOrUndefined(townCity) && !GlobalFunctions.isNullOrUndefined(this.entity.cityTown)) {
                if (townCity.name === this.entity.cityTown.name) {
                  this.group.controls['town-city'].setValue(townCity);
                }
              }
          });
        }
      }
    }

    public compareFn(option, selected): boolean {
      return !isNullOrUndefined(option) && !isNullOrUndefined(selected) ?
        option.id === selected.id : false;
    }

    public switchMarkForDeletion() {
      this.entity.markForDeletion = !this.entity.markForDeletion;
    }
}
