import {
  Component,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy
} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { GridSort } from './grid-sort.model';
import { GridHeaderModel } from './grid-header.model';
import * as moment from 'moment';
import * as _ from 'lodash';
// import { TableHeaderTranslations } from '../../stockcount/translations/table-header-translations.model';
// import { DateRangesTranslations } from '../translations/date-ranges-translations.model';
// import { DateTimeSettingsService, DateFormat } from '../date-time-settings.service';
import { DoCheck } from '@angular/core/src/metadata/lifecycle_hooks';
import { Router } from '@angular/router';
import { GlobalFunctions } from '../../../global/global-function.constant';
import { isBoolean } from 'util';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class GridComponent implements DoCheck {
  itemsPerPage: number;
  @Input() ignoreClick: boolean;
  @Output() openEmitter: EventEmitter<any>;
  @Output() deleteEmitter: EventEmitter<any>;
  @Output() addEmitter: EventEmitter<any>;
  @Input() heading: string;
  @Input() data: Observable<any[]>;
  @Input() headers: GridHeaderModel[];
  @Input() tableHeaderTranslations: any;
  //  @Input() dateRangesTranslations: DateRangesTranslations;
  @Input() page: Number;
  @Input() isBusy: boolean;
  @Input() gridLinkColor: string;

  public query: string;
  public currentQuery: GridSort;
  public markForDeletion: boolean;
  @Input() paginationId: string;

  // constructor(private dateTimeSettingsService: DateTimeSettingsService) {
  constructor() {
    this.currentQuery = new GridSort('', 'asc');
    this.addEmitter = new EventEmitter<any>();
    this.deleteEmitter = new EventEmitter<any>();
    this.openEmitter = new EventEmitter<any>();
    this.itemsPerPage = 20;
  }

  ngDoCheck() {
      this.isMarkedForDeletion();
  }

  public getTranslation(translationKey) {
    return this.tableHeaderTranslations[translationKey];
  }

  public sortTable(field: GridHeaderModel) {
    this.page = 1;
    this.query = this.currentQuery.getSortDescription(field);
  }

  public formatDate(date: any) {
    return moment(date).format('Do MMM YY');
  }

  public getValue(value: any, header: GridHeaderModel) {
    const proporties = header.propertyName.split('.');
    if (!GlobalFunctions.isNullOrUndefined(value)) {
      let values: any = value;
      proporties.forEach(propertyName => {
        if (!GlobalFunctions.isNullOrUndefined(values[propertyName])) {
          values = values[propertyName];
        } else {
          values = '';
        }
      });
      return values;
    }
    return '';
  }

  public isMarkedForDeletion() {
    this.markForDeletion = false;
    if (!GlobalFunctions.isNullOrUndefined(this.data)) {
      this.data.subscribe(x => {
        const result = _.filter(x, ['selected', true]);
        this.markForDeletion = result.length > 0;
      });
    }
  }

  public parseBool(val: any): boolean {
    if (isBoolean(val)) {
      return (val === 'true');
    }
    return false;
  }

  public edit(dataItem: any) {
    if (!this.ignoreClick) {
      this.openEmitter.emit(dataItem);
    }
  }

  public checkBoxOver() {
    this.ignoreClick = true;
  }

  public checkBoxOut() {
    this.ignoreClick = false;
  }
}
