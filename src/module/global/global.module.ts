import { NgModule } from '@angular/core';
import { GlobalFunctions } from './global-function.constant';
import { CustomHttp } from './custom-http.service';
import { GridComponent } from './component/grid/grid.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgxPaginationModule } from 'ngx-pagination';
import { GridSortPipe } from './component/grid/grid-sort.pipe';
import { RouterModule } from '@angular/router';
import { BusyIndicatorComponent } from './component/busy-indicator/busy-indicator.component';
import { GenericService } from './service/generic.service';
// import { GenericService } from './service/generic.service';
import { GenericSmartComponent } from './component/generic-smart/generic-smart.component';
import { HttpClientModule  } from '@angular/common/http';

@NgModule({
  imports: [CommonModule,
            NgxPaginationModule,
            RouterModule,
            FormsModule,
            ReactiveFormsModule, HttpClientModule ],
  declarations: [GridComponent, GridSortPipe, BusyIndicatorComponent, GenericSmartComponent],
  exports: [GridComponent, GridSortPipe, GenericSmartComponent],
  providers: [GlobalFunctions, CustomHttp, GridSortPipe, GenericService]
})
export class GlobalModule {}
