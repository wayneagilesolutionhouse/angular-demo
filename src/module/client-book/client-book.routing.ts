import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientsComponent } from './component/clients/clients.component';
import { ContactsComponent } from './component/contacts/contacts.component';
import { AddressesComponent } from './component/addresses/addresses.component';
import { TownCitiesComponent } from './component/town-cities/town-cities.component';
import { CountiesComponent } from './component/counties/counties.component';
import { RegionsComponent } from './component/regions/regions.component';
import { CountriesComponent } from './component/countries/countries.component';

// Guards
// import { AuthGuard } from './guard/auth.gaurd';

const routes: Routes = [
  { path: 'clients', component: ClientsComponent },
  { path: 'contacts', component: ContactsComponent },
  { path: 'addresses', component: AddressesComponent },
  { path: 'towns', component: TownCitiesComponent },
  { path: 'counties', component: CountiesComponent },
  { path: 'regions', component: RegionsComponent },
  { path: 'countries', component: CountriesComponent },
];

export const ClientBookRouting: ModuleWithProviders = RouterModule.forChild(routes);
