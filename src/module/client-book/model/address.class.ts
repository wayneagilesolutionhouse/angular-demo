﻿import { CityTown } from './city-town.class';
import { GlobalFunctions } from '../../Global/global-function.constant';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ModelBase } from '../../Global/model/model-base.class';

export class Address extends ModelBase {
    constructor() {
        super();
        this.path = 'ClientBook/addresses';
    }

    public id: number;
    public houseNo: string;
    public street: string;
    public area: string;
    public cityTown: CityTown;
    public postCode: string;

    static instantiate = (id, houseNo: string, street: string, postCode: string, cityTown: CityTown) => {
        const returnValue = new Address();
        returnValue.id = id;
        returnValue.houseNo = houseNo;
        // returnValue.area = area;
        returnValue.street = street;
        returnValue.postCode = postCode;
        returnValue.cityTown = cityTown;
        return returnValue;
    }

    static mapFormToModel = (complexForm: FormGroup, id: number): Address => {
      const entity = new Address();
      return entity.mapFormToModel(complexForm, id);
    }

    static mapModelToServiceModel(model: Address): any {
      const entity = new Address();
      return entity.mapModelToServiceModel(model);
    }

    static mapServiceModelToModel(serviceModel: any): Address {
      const entity = new Address();
      return entity.mapServiceModelToModel(serviceModel);
    }

    public mapModelToServiceModel(model: Address): any {
      return GlobalFunctions.isNullOrUndefined(model) ? null : {
          // Name: model.name,
          ID: model.id,
          Area: model.area,
          HouseNo: model.houseNo,
          Street: model.street,
          Active: !model.markForDeletion,
          TownId: model.cityTown.id,
          // CreationDate: model.creationDate,
          PostCode: model.postCode
      };
    }

    public mapServiceModelToModel(serviceModel: any): Address {
        const address = new Address();
        address.id = serviceModel.id;
        address.markForDeletion = !serviceModel.active;
        address.houseNo = serviceModel.houseNo;
        serviceModel.area = serviceModel.area;
        address.street = serviceModel.street;
        address.postCode = serviceModel.postCode;
        address.cityTown = !GlobalFunctions.isNullOrUndefined(serviceModel.town) ? serviceModel.town : null;
        return address;
    }

  public mapServiceModelsToModels(servicesModel: any) {
    const returnValue: Address[] = [];

    if (Array.isArray(servicesModel)) {
        servicesModel.forEach(serviceModel => {
            returnValue.push(this.mapServiceModelToModel(serviceModel));
        });
    }
    return returnValue;
  }

  public mapFormToModel = (complexForm: FormGroup, id: number): Address => {
        const returnValue = new Address();
        returnValue.area = !GlobalFunctions.isNullOrUndefined(complexForm.get('area').value) ? complexForm.get('area').value : null;
        returnValue.houseNo = !GlobalFunctions.isNullOrUndefined(complexForm.get('house-no').value) ?
        complexForm.get('house-no').value : null;
        returnValue.street = !GlobalFunctions.isNullOrUndefined(complexForm.get('street').value) ? complexForm.get('street').value : null;
        returnValue.cityTown = !GlobalFunctions.isNullOrUndefined(complexForm.get('town-city').value) ?
        complexForm.get('town-city').value : null;
        returnValue.postCode = !GlobalFunctions.isNullOrUndefined(complexForm.get('post-code').value) ?
        complexForm.get('post-code').value : null;
        returnValue.id = id;
        return returnValue;
    }
}
