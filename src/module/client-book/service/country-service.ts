﻿import { Injectable } from '@angular/core';
import { Country } from '../model/country.class';
import { GlobalFunctions } from '../../Global/global-function.constant';
import { CustomHttp } from '../../global/custom-http.service';
import { Observable } from 'rxjs/Observable';
import { ConnectionBackend, XHRBackend, RequestOptions, Request, Response, Http, Headers } from '@angular/http';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { tap, catchError } from 'rxjs/operators';

@Injectable()
export class CountryService {
    static readonly COUNTRY_WEBAPI: string = 'api/ClientBook';

    constructor(customHttp: CustomHttp) {
        this.customHttp = customHttp;
    }

    private customHttp: CustomHttp;

    public getAll(): Observable<Country[]> {
        return this.customHttp.get(CountryService.COUNTRY_WEBAPI + '/' + 'Countries').map(x => {
            return Country.mapServiceModelsToModels(x.json());
        });
    }

    public get(id: number): Country {
        // let result = this.countries.filter(item => item.id === id);
        // return result.length ? result[0] : null;
        return null;
    }
    public getByName(name: string): Country {
        // let result = this.countries.filter(item => item.name === name);
        // return result.length ? result[0] : null;
        return null;
    }
    public save(entity: Country): Observable<Country> {
        return this.customHttp.post(CountryService.COUNTRY_WEBAPI + '/' + 'Countries',
            JSON.stringify(Country.mapModelToServiceModel(entity))).map(x => {
            return Country.mapServiceModelToModel(x.json());
        });
    }
    public update(entity: Country): Observable<Country> {
        return this.customHttp.put(CountryService.COUNTRY_WEBAPI + '/' + 'Countries' + '/' + entity.id ,
        JSON.stringify(Country.mapModelToServiceModel(entity)), null).map(x => {
            return Country.mapServiceModelToModel(x.json());
        });
    }
    public delete(entity: Country): Observable<any> {
        entity.selected = false;
        return this.customHttp.delete(CountryService.COUNTRY_WEBAPI + '/' + 'Countries' + '/' + entity.id);
    }

    public deleteAll(countries: Country[]): Observable<any> {
        const arr: any[] = [];
        const result = countries.filter(country => country.selected === true);
        result.forEach(country => {
            arr.push(this.delete(country).pipe(
              tap(x => {
                console.log('country ' + country.id + '' + 'delete saved');
            })));
        });
        return forkJoin(arr).map(res => res);
    }

    public markForDeletion(countries: Country[]): boolean {
        if (!GlobalFunctions.isNullOrUndefined(countries)) {
            const result = countries.filter(country => country.selected === true);
            return !GlobalFunctions.isNullOrUndefined(result) && result.length > 0;
        }
        return false;
    }
}
