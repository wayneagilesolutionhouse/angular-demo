﻿import { Component, Input, OnInit } from '@angular/core';
import { Region } from '../../../model/region.class';
import { Country } from '../../../model/country.class';
import { County } from '../../../model/county.class';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { CountryService } from '../../../service/country-service';
import { RegionService } from '../../../service/region.service';
import { GenericService } from '../../../../Global/service/generic.service';
// import { CountyService } from '../../../service/county.service';
import { GlobalFunctions } from '../../../../Global/global-function.constant';
import { Observable } from 'rxjs/Observable';
import { isNullOrUndefined } from 'util';

export interface CountyModel {
    title: string;
    county: County;
}

@Component({
    selector: 'app-county',
    templateUrl: './county.component.html'
})

export class CountyComponent extends DialogComponent<CountyModel, boolean> implements CountyModel, OnInit {
    complexForm: FormGroup;
    _county: County;
    title: string;
    public counties: County[] = [];
    public countries: Observable<Country[]>;
    public regions: Observable<Region[]>;

    constructor(dialogService: DialogService, fb: FormBuilder,
      private countyService: GenericService, private countryService: CountryService,
      private regionService: RegionService) {
      super(dialogService);
      this.complexForm = fb.group({
        'name': [null, Validators.required],
          'region': [null, Validators.required],
          'country': [null, Validators.required]
      });
    }

    ngOnInit() {
      this.countries = this.countryService.getAll();
      this.countries.subscribe(countries => countries.forEach(x => {
        if (!GlobalFunctions.isNullOrUndefined(x) &&
        !GlobalFunctions.isNullOrUndefined(this._county.region) && !GlobalFunctions.isNullOrUndefined(this._county.region.country)) {
            if (x.name === this._county.region.country.name) {
                this.complexForm.controls['country'].setValue(x);
            }
        }
      }));

      this.regions = this.regionService.getAll();
      this.regions.subscribe(x => x.forEach(region => {
        if (!GlobalFunctions.isNullOrUndefined(region) && !GlobalFunctions.isNullOrUndefined(this._county.region)) {
            if (region.name === this._county.region.name) {
                this.complexForm.controls['region'].setValue(region);
            }
        }
      }));
    }

    get county(): County {
        return this._county;
    }
    set county(c: County) {
        this._county = c;
        this.complexForm.controls['name'].setValue(c.name);
    }

    public switchMarkForDeletion() {
      this.county.markForDeletion = !this.county.markForDeletion;
    }

    public submitForm(value) {
        this._county = County.mapFormToModel(this.complexForm, this._county.id);
        const outcome = this.county.id > 0 ? this.countyService.update(this.county).subscribe(x => {
          if (!GlobalFunctions.isNullOrUndefined(x)) {
              this.result = true;
              this.close();
          }
        }) : this.countyService.save(this.county).subscribe(x => {
          if (!GlobalFunctions.isNullOrUndefined(x)) {
              this.result = true;
              this.close();
          }
      });
    }

    public compareFn(option, selected): boolean {
      return !isNullOrUndefined(option) && !isNullOrUndefined(selected) ?
        option.id === selected.id : false;
    }
}
