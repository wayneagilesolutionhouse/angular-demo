﻿import { Component, Input } from '@angular/core';
import { Region } from '../../../model/region.class';
import { Country } from '../../../model/country.class';
import { Contact } from '../../../model/contact.class';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GlobalFunctions } from '../../../../../global/global-functions.constants';
import { isNullOrUndefined } from 'util';

export interface ContactModel {
    title: string;
    contact: Contact;
}

@Component({
    selector: 'app-contact-dumb',
    templateUrl: './contact-dumb.component.html'
})

export class ContactDumbComponent {
    title: string;
    @Input() group: FormGroup;
    entity: Contact;

    @Input() set contact(c: Contact) {
        this.entity = c;
        if (!GlobalFunctions.isNullOrUndefined(c)) {
          this.group.controls['id'].setValue(c.id);
          this.group.controls['firstname'].setValue(c.firstname);
          this.group.controls['surname'].setValue(c.surname);
          this.group.controls['mobile'].setValue(c.mobile);
          this.group.controls['telephone'].setValue(c.telephone);
          this.group.controls['email'].setValue(c.email);
          this.group.controls['skype'].setValue(c.skype);
          this.group.controls['website'].setValue(c.website);
        }
    }
    constructor() {
    }

    public switchMarkForDeletion() {
      this.entity.markForDeletion = !this.entity.markForDeletion;
    }

    public compareFn(option, selected): boolean {
      return !isNullOrUndefined(option) && !isNullOrUndefined(selected) ?
        option.id === selected.id : false;
    }
}


