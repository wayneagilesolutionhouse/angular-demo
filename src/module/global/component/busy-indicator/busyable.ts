import {EventEmitter} from '@angular/core';

/**
 * Things which are loading should be in an unusable state. If they are
 * still usable, then don't expose "loading"
 */
export interface ExposeLoading {
    loading: EventEmitter<boolean>;
}
