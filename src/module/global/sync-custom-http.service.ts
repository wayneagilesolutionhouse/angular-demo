import { Injectable } from '@angular/core';
import { ConnectionBackend, XHRBackend, RequestOptions, Request, RequestOptionsArgs, Response, Http, Headers } from "@angular/http";
import { config } from './config';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

@Injectable()
export class SyncCustomHttp {// extends Http {
    constructor(private http: Http, backend: XHRBackend, defaultOptions: RequestOptions) {
       // super(backend, defaultOptions);
       http = new Http(backend, defaultOptions);
    }

    async get(url: string, options?: RequestOptionsArgs): Promise<any> {
        return await this.http.get(config.serviceUrl + '/' + url, new RequestOptions()).catch(this.handleError);
    }

    async post(url: string, body: string, options?: RequestOptionsArgs):  Promise<any> {
      return await this.post(config.serviceUrl + '/' + url, body, this.addJwt(options)).catch(this.handleError);
    }

    async put(url: string, body: string, options?: RequestOptionsArgs):  Promise<any> {
      return await this.put(config.serviceUrl + '/' + url, body, this.addJwt(options)).catch(this.handleError);
    }

    async delete(url: string, options?: RequestOptionsArgs):  Promise<any> {
      return await this.delete(config.serviceUrl + '/' + url, this.addJwt(options)).catch(this.handleError);
    }

    // private helper methods
    private addJwt(options?: RequestOptionsArgs): RequestOptionsArgs {
        // ensure request options and headers are not null
        options = options || new RequestOptions();
        options.headers = options.headers || new Headers();
        options.headers.append('Accept', 'application/json, text/plain, */*');
        options.headers.append('Content-Type', 'application/json;charset=UTF-8');
        options.headers.append('Response-Type', 'json');

      //  { responseType: "text" }.
        return options;
    }

    private handleError(error: any) {
        if (error.status === 401) {
            window.location.href = '/login';
        }

        return Observable.throw(error._body);
    }
}

export function customHttpFactory(xhrBackend: XHRBackend, requestOptions: RequestOptions): Http {
   // return new CustomHttp(xhrBackend, requestOptions);
   return null;
}

export let customHttpProvider = {
    provide: Http,
    useFactory: customHttpFactory,
    deps: [XHRBackend, RequestOptions]
};
