﻿import { User } from './user.class';
import * as moment from 'moment';

export class ModelBase  {
    constructor() {
        this.id = 0;
        this.creationDate = moment(new Date()).format('DD/MM/YYYY');
        this.modificationDate = moment(new Date()).format('DD/MM/YYYY');
        this.markForDeletion = false;
        this.selected = false;
    }
    public id: number;
    public name: string;
    public creationDate: string;
    public modificationDate: string;
    public userCreated: User;
    public userModified: User;
    public selected: boolean;
    public markForDeletion: boolean;
    public path: string;
}
