import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class HttpInterceptionService {

  constructor() { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const clonedRequest = req.clone({
      responseType: 'text'
    });

    return next.handle(clonedRequest)
      .map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          return event.clone({
            body: JSON.parse(event.body),
          });
        }
      })
      .catch((error: HttpErrorResponse) => {
          const parsedError = Object.assign({}, error, { error: JSON.parse(error.error) });
          return Observable.throw(new HttpErrorResponse(parsedError));
      });
  }

}
