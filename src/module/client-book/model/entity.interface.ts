export interface IEntity {
  id: number;
  selected: boolean;
  mapModelToServiceModel(model: IEntity): any;
  mapServiceModelToModel(serviceModel: any): IEntity;
  mapServiceModelsToModels(servicesModel: any[]);
}
