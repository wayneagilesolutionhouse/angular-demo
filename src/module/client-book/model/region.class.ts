﻿
import { Country } from './country.class';
import { GlobalFunctions } from '../../Global/global-function.constant';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ModelBase } from '../../Global/model/model-base.class';

export class Region extends ModelBase {

    constructor(id: number, region: string, country: Country) {
        super();
        this.id = id;
        this.name = region;
        this.country = country;
        this.path = 'ClientBook/regions';
    }
    public country: Country;

    static mapModelToServiceModel(model: Region): any {
      return GlobalFunctions.isNullOrUndefined(model) ? null : {
          Name: model.name,
          ID: model.id,
          CountryId: !GlobalFunctions.isNullOrUndefined(model.country) ? model.country.id : 0,
          Active: !model.markForDeletion
      };
    }

    static mapServiceModelToModel(serviceModel: any): Region {
        const region = new Region(serviceModel.id, serviceModel.name, serviceModel.country);
        region.country = Country.mapServiceModelToModel(region.country);
        region.markForDeletion = !serviceModel.active;
        return region;
    }

  static mapServiceModelsToModels(servicesModel: any) {
    const returnValue: Region[] = [];

    if (Array.isArray(servicesModel)) {
        servicesModel.forEach(serviceModel => {
            returnValue.push(Region.mapServiceModelToModel(serviceModel));
        });
    }
    return returnValue;
  }

    static mapFormToModel = (complexForm: FormGroup, id: number): Region => {
        const name = !GlobalFunctions.isNullOrUndefined(complexForm.get('name').value) ? complexForm.get('name').value : null;
        const country = !GlobalFunctions.isNullOrUndefined(complexForm.get('country').value) ? complexForm.get('country').value : null;
        return new Region(id, name, country);
    }

    public mapModelToServiceModel(model: Region): any {
      return GlobalFunctions.isNullOrUndefined(model) ? null : {
          Name: model.name,
          ID: model.id,
          CountryId: !GlobalFunctions.isNullOrUndefined(model.country) ? model.country.id : 0,
          Active: !model.markForDeletion
      };
    }
}
