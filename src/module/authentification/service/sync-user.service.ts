import { Injectable } from '@angular/core';
import { ConnectionBackend, XHRBackend, RequestOptions, Request, Response, Http, Headers } from '@angular/http';
import { User } from '../model/user';
import { Observable } from 'rxjs/Observable';
import { SyncCustomHttp } from '../../global/sync-custom-http.service';
import { UserService } from '../../authentification/service/user.service';
import { GlobalFunctions } from '../../global/global-function.constant';
@Injectable()
export class SyncUserService {
  static readonly COUNTRY_WEBAPI: string = 'api/CredentialBook';
  private users: User[];
  constructor(private syncCustomHttp: SyncCustomHttp) {
    this.users = [];
  }
  // async getPrice(currency: string): Promise<number> {
  //   const response = await this.http.get(this.currentPriceUrl).toPromise();
  //   return response.json().bpi[currency].rate;
  // }

  public async update(user: User) {
    const id = !GlobalFunctions.isNullOrUndefined(user._id) ? user._id : 0;
    const loginModel = {
      email: user.username,
      rememberMe: true,
      token: user.token,
      password: user.password
    };
    const result = await this.syncCustomHttp.put(UserService.COUNTRY_WEBAPI + '/' + 'Account' + '/' + id ,
    JSON.stringify(loginModel), null);
    return User.mapServiceLoginModelToModel(result.json());
 }
}
