import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { GlobalFunctions } from '../../../Global/global-function.constant';
import { ModelBase } from '../../model/model-base.class';
import { GenericService } from '@global/service/generic.service';

export class GenericComponent<T> extends DialogComponent<T, boolean>
  implements OnInit {
  public model: any;
  public complexForm: FormGroup;
  public processed: Number;
  public isBusy: boolean;
  public returnTrue: boolean;
  @Output() closeProperties: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(
    public dialogService: DialogService,
    public genericService: GenericService
  ) {
    super(dialogService);
    this.loadProcessed = 0;
    this.returnTrue = false;
  }

  ngOnInit() {}

  public set loadProcessed(value: Number) {
    this.processed = value;
  }
  public get loadProcessed(): Number {
    return this.processed;
  }

  public busy(total: number): boolean {
    return this.loadProcessed < total;
  }
  public submitForm(value) {
    this.isBusy = true;
    this.result = false;
    this.model = this.model.mapFormToModel(this.complexForm, this.model.id);
    const outcome =
      this.model.id > 0
        ? this.genericService.update(this.model).subscribe(x => {
            this.isBusy = false;
            this.result = true;
            this.close();
          })
        : this.genericService.save(this.model).subscribe(x => {
            this.isBusy = false;
            this.result = true;
            this.close();
          });
  }

  @Input()
  get entity(): ModelBase {
    return this.model;
  }
  set entity(a: ModelBase) {
    if (a.id) {
      this.isBusy = true;
      this.genericService.get(a).subscribe(x => {
        this.model = !GlobalFunctions.isNullOrUndefined(x[0]) ? x[0] : null;
        this.isBusy = false;
        this.incrementProcess();
      });
    } else {
      this.incrementProcess();
      this.model = a;
    }
  }

  public close() {
    if (this.closeProperties.observers.length) {
      this.closeProperties.emit(false);
    } else {
      super.close();
    }
  }

  public incrementProcess() {
    this.loadProcessed = parseInt(this.loadProcessed.toString(), 0) + 1;
  }
}
