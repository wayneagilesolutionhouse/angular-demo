﻿import { Injectable } from '@angular/core';
import { ConnectionBackend, XHRBackend, RequestOptions, Request, Response, Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { CustomHttp } from '../../global/custom-http.service';
import 'rxjs/add/operator/map';
import { LocalStorageService } from 'angular-2-local-storage';
import { User } from '../model/user';
import { UserService } from '../service/user.service';
import 'rxjs/add/observable/of';
import { GlobalFunctions } from '../../global/global-function.constant';
import { error } from 'util';

@Injectable()
export class AuthenticationService {
    private customHttp: CustomHttp;
    constructor(private http: Http, private localStorageService: LocalStorageService,
                private userService: UserService, customHttp: CustomHttp) {
      this.customHttp = customHttp;
    }

    public loginWithBearer() {
      const userModel = JSON.parse(this.localStorageService.get<string>('currentUser'));
      if (!GlobalFunctions.isNullOrUndefined(userModel)) {
        return this.userService.update(userModel);
      }  else {
        throw new error('No user details in local');
      }
    }

    public login(username: string, password: string) {
      const user = new User();
      user.password = password;
      user.username = username;

      return this.userService.update(user);
    }

    logout() {
      const userLocalStorage = JSON.parse(this.localStorageService.get<string>('currentUser'));
      if (!GlobalFunctions.isNullOrUndefined(userLocalStorage) && !GlobalFunctions.isNullOrUndefined(userLocalStorage.token)) {
        this.userService.delete(userLocalStorage).subscribe(token => {
          if (token) {
            this.localStorageService.remove('currentUser');
          }
      });
    }
  }
}
