import { Component } from '@angular/core';
import { AuthenticationService } from '../../module/authentification/service/authentification.service';
import { LocalStorageService } from '../../module/authentification/service/local-storage.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-nav-menu',
    templateUrl: './navmenu.component.html',
    styleUrls: ['./navmenu.component.scss']
})
export class NavMenuComponent {
  public menu = {
      crmClientBook : {
      visible : false
    },
      businessCompositions: {
      visible: false
    },
    eccommerce: {
        visible: false
    }
};
    public constructor(private authenticationService: AuthenticationService, private router: Router) {
    }

    public logOut() {
        this.authenticationService.logout();
        this.router.navigateByUrl('/login');
    }
}
