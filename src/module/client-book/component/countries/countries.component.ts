﻿import { Component, OnInit } from '@angular/core';
import { Country } from '../../model/country.class';
import { CountryComponent } from './country/country.component';
import { DialogService } from 'ng2-bootstrap-modal';
import { CountryService } from '../../service/country-service';
import { Observable } from 'rxjs/Observable';
import { GenericService } from '@global/service/generic.service';

@Component({
    selector: 'app-countries',
    templateUrl: './countries.component.html'
})
export class CountriesComponent implements OnInit {
    public countries: Country[];
    public selection: Country;
    public show: boolean;
    private ignoreClick: boolean;

    constructor(private dialogService: DialogService, private countryService: GenericService) {
    }

    ngOnInit() {
        this.countries = [];
        this.countryService.getAll(Country.instantiate()).subscribe(x => {
            this.countries = x;
        });
    }

    public showDetails(selected: Country) {
        const self = this;
        if (!this.ignoreClick) {
            const disposable = this.dialogService.addDialog(CountryComponent, {
                title: 'Country',
                country: selected
            })
                .subscribe((isConfirmed) => {
                    if (isConfirmed) {
                        this.countries = [];
                        this.countryService.getAll(Country.instantiate()).subscribe(x => {
                            this.countries = x;
                        });
                    }
                });
        }
    }

    public openCreate = () => {
        this.showDetails(new Country('', 0));
    }

    public checkBoxOver() {
        this.ignoreClick = true;
    }

    public checkBoxOut() {
        this.ignoreClick = false;
    }

    public deleteAll = () => {
        const self = this;
        self.countryService.deleteAll(this.countries).subscribe(x => {
          this.countryService.getAll(Country.instantiate()).subscribe(countries => {
            this.countries = countries;
          }, e => {
            console.log(e);
          });
        });
    }

    public isMarkedForDeletion(): boolean {
        // let returnValue: boolean;
        //     returnValue = this.countryService.markForDeletion(this.countries);
        // return returnValue;
        return false;
    }
}
