﻿import { Component, Input } from '@angular/core';
import { Region } from '../../../model/region.class';
import { Country } from '../../../model/country.class';
import { Contact } from '../../../model/contact.class';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { GlobalFunctions } from '@global/global-function.constant';
import { ModelBase } from '@global/model/model-base.class';
import { GenericComponent } from '@global/component/generic/generic.component';
import { GenericService } from '@global/service/generic.service';

export interface ContactModel {
    title: string;
    entity: ModelBase;
}

@Component({
    selector: 'app-contact-smart',
    templateUrl: './contact-smart.component.html'
})

export class ContactSmartComponent extends GenericComponent<ContactModel> implements ContactModel {
  // public contactModel: Contact;
  public title: string;
  public isBusy: boolean;

  constructor(dialogService: DialogService, fb: FormBuilder, genericService: GenericService) {
      super(dialogService, genericService);
      this.isBusy = false;
      this.complexForm = fb.group({
          'id': [null],
          'firstname': [null, Validators.required],
          'surname': [null, Validators.required],
          'mobile': [null, Validators.required],
          'telephone': [null, Validators.required],
          'email': [null, Validators.required],
          'skype': [null],
          'website': [null, Validators.required]
      });
  }
}
