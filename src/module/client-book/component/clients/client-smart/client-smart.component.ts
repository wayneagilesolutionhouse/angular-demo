﻿import { Component, Input, EventEmitter, OnInit } from '@angular/core';
import { Address } from '../../../model/address.class';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { Client } from '../../../model/client.class';
import { CityTown } from '../../../model/city-town.class';
import { GlobalFunctions } from '../../../../Global/global-function.constant';
import { Observable } from 'rxjs/Observable';
import { tap, catchError } from 'rxjs/operators';
import { ModelBase } from '@global/model/model-base.class';
import { GenericComponent } from '@global/component/generic/generic.component';
import { GenericService } from '@global/service/generic.service';

export interface ClientModel {
    title: string;
    entity: ModelBase;
    saveSucceeded: EventEmitter<string>;
}

@Component({
    selector: 'app-client-smart',
    templateUrl: './client-smart.component.html'
})

export class ClientSmartComponent extends GenericComponent<ClientModel> implements ClientModel, OnInit {
  cityTowns: CityTown[];
    complexForm: FormGroup;
    clientModel: Client;
    title: string;
    saveSucceeded: EventEmitter<string>;
    public isBusy: boolean;

    constructor(dialogService: DialogService, fb: FormBuilder,
      genericService: GenericService) {
    super(dialogService, genericService);

        this.isBusy = true;
        this.complexForm = fb.group({
            name: ['', [Validators.required, Validators.minLength(2)]],
            address: fb.group({
                'id': [null],
                'house-no': [null, Validators.required],
                'street': [null, Validators.required],
                'town-city': [null, Validators.required],
                'area': [null],
                'post-code': [null, Validators.required]
            }),
            contact: fb.group({
                'id': [null],
                'firstname': [null, Validators.required],
                'surname': [null, Validators.required],
                'mobile': [null, Validators.required],
                'telephone': [null, Validators.required],
                'email': [null, Validators.required],
                'skype': [null],
                'website': [null, Validators.required]
            })
        });
    }

    ngOnInit() {
      this.isBusy = true;
      const city = new CityTown('');
      this.genericService.getAll(city).subscribe(result => {
        this.cityTowns =  result;
        this.incrementProcess();
          this.isBusy = false;
      });
    }
}
