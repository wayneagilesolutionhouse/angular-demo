﻿export class User {
    _id: string;
    username: string;
    password: string;
    confirmPassword: string;
    userTypeId: number;
    firstName: string;
    lastName: string;
    token: string;

    public static parse(serialisation: any): User {
      const user = JSON.parse(serialisation);
      return User.mapServiceLoginModelToModel(serialisation);
    }

    public static mapServiceModelsToModels(serviceModels: any[]): User[] {
      return null;
    }

    public static mapModelToServiceModel(model: User): any {
      return {
        Token: model.token,
        Email: model.username,
        Password: model.password,
        UserTypeId: model.userTypeId,
        ConfirmPassword: model.confirmPassword
      };
    }

    public static mapServiceLoginModelToModel(loginServiceModel: any) {
      const returnValue = new User();
      if (loginServiceModel !== null) {
        returnValue.token = loginServiceModel.token;
        returnValue.username = loginServiceModel.email;
        returnValue.userTypeId = loginServiceModel.userTypeId;
      }
      return returnValue;
    }

    public static mapServiceModelToModel(serviceModel: any): User {
      const returnValue = new User();
      returnValue.username = serviceModel.Email;
      returnValue.userTypeId = serviceModel.UserTypeId;
      returnValue.password = serviceModel.Password;
      return returnValue;
    }
}
