import { Injectable } from '@angular/core';
import { GlobalFunctions } from '../global-function.constant';
import { CustomHttp } from '../../global/custom-http.service';
import { Observable } from 'rxjs/Observable';
import { ConnectionBackend, XHRBackend, RequestOptions, Request, Response, Http, Headers } from '@angular/http';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { tap, catchError } from 'rxjs/operators';

@Injectable()
export class GenericService {
   // static readonly WEBAPI: string = 'api/ClientBook';
   static readonly WEBAPI: string = 'api';

    constructor(customHttp: CustomHttp) {
        this.customHttp = customHttp;
    }

    private customHttp: CustomHttp;

    public getAll(entity: any): Observable<any[]> {
        console.log('');
        console.log('');
        console.log('');
        return this.customHttp.get(GenericService.WEBAPI + '/' + entity.path).map(x => {
            return entity.mapServiceModelsToModels(x.json());
        });
    }

    public get(entity: any): Observable<any> {
      return this.customHttp.get(GenericService.WEBAPI + '/' + entity.path + '/' + entity.id.toString()).map(x => {
        return entity.mapServiceModelsToModels(x.json());
      });
    }
    public getByName(name: string): any {
        // let result = this.countries.filter(item => item.name === name);
        // return result.length ? result[0] : null;
        return null;
    }
    public save(entity: any): Observable<any> {
      console.log('');
      console.log('');
      console.log('');
        return this.customHttp.post(GenericService.WEBAPI + '/' + entity.path,
            JSON.stringify(entity.mapModelToServiceModel(entity)));
    }
    public update(entity: any): Observable<any> {
      console.log('');
      console.log('');
      console.log('');
        return this.customHttp.put(GenericService.WEBAPI + '/' + entity.path + '/' + entity.id ,
        JSON.stringify(entity.mapModelToServiceModel(entity)), null);
    }
    public delete(entity: any): Observable<any> {
        entity.selected = false;
        return this.customHttp.delete(GenericService.WEBAPI + '/' + entity.path + '/' + entity.id);
    }

    public deleteAll(entities: any[]): Observable<any> {
        const arr: any[] = [];
        const result = entities.filter(entity => entity.selected === true);
        result.forEach(entity => {
            arr.push(this.delete(entity).pipe(
              tap(x => {
                console.log('country ' + entity.id + '' + 'delete saved');
            })));
        });
        return forkJoin(arr).map(res => res);
    }
}
