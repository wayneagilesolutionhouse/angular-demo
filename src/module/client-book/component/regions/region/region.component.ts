﻿import { Component, Input, OnInit } from '@angular/core';
import { Region } from '../../../model/region.class';
import { Country } from '../../../model/country.class';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { CountryService } from '../../../service/country-service';
import { RegionService } from '../../../service/region.service';
import { GlobalFunctions } from '../../../../Global/global-function.constant';
import { Observable } from 'rxjs/Observable';
import { isNullOrUndefined } from 'util';
import { GenericComponent } from '@global/component/generic/generic.component';
import { GenericService } from '@global/service/generic.service';
export interface RegionModel {
    title: string;
    region: Region;
}

@Component({
    selector: 'app-region',
    templateUrl: './region.component.html'
})

export class RegionComponent extends GenericComponent<RegionModel> implements RegionModel, OnInit {
    complexForm: FormGroup;
    _region: Region;
    title: string;
    get region(): Region {
        return this._region;
    }
    set region(r: Region) {
        this._region = r;
        this.complexForm.controls['name'].setValue(r.name);
        this.complexForm.controls['country'].setValue(r.country);
    }

    public countries: Country[];
    public regions: Region[] = [];
    constructor(dialogService: DialogService, fb: FormBuilder, private regionService: GenericService) {
        super(dialogService, regionService);
        this.complexForm = fb.group({
          'name': [null, Validators.required],
          'country': [null, Validators.required]
        });
    }

    ngOnInit() {
      const country = new Country('', 0);
      this.genericService.getAll(country).subscribe(result => {
        this.countries =  result;
        this.incrementProcess();
      });
    }

    public switchMarkForDeletion() {
      this.region.markForDeletion = !this.region.markForDeletion;
    }

    public compareFn(option, selected): boolean {
      return !isNullOrUndefined(option) && !isNullOrUndefined(selected) ?
        option.id === selected.id : false;
    }

    public submitForm(value) {
      this.region = Region.mapFormToModel(this.complexForm, this.region.id);
      const outcome = this.region.id > 0 ? this.regionService.update(this.region).subscribe(x => {
          if (!GlobalFunctions.isNullOrUndefined(x)) {
              this.result = true;
              this.close();
          }
      }) : this.regionService.save(this.region).subscribe(x => {
          if (!GlobalFunctions.isNullOrUndefined(x)) {
              this.result = true;
              this.close();
          }
      });
      this.result = false;
  }
}
