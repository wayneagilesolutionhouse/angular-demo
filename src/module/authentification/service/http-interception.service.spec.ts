import { TestBed, inject } from '@angular/core/testing';

import { HttpInterceptionService } from './http-interception.service';

describe('HttpInterceptionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HttpInterceptionService]
    });
  });

  it('should be created', inject([HttpInterceptionService], (service: HttpInterceptionService) => {
    expect(service).toBeTruthy();
  }));
});
