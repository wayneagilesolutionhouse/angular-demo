﻿import { Component, OnInit } from '@angular/core';
import { County } from '../../model/county.class';
import { CountyComponent } from './county/county.component';
import { DialogService } from 'ng2-bootstrap-modal';
import { CountyService } from '../../service/county.service';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'app-counties',
    templateUrl: './counties.component.html'
})
export class CountiesComponent implements OnInit {
    public counties: County[];
    private ignoreClick: boolean;

    constructor(private dialogService: DialogService, private countyService: CountyService) {
    }

    ngOnInit() {
      const counties = this.countyService.getAll().subscribe(x => {
        this.counties = x;
      });
    }

    public showDetails(selected: County) {
        if (!this.ignoreClick) {
            const disposable = this.dialogService.addDialog(CountyComponent, {
                title: 'County',
                county: selected
            })
                .subscribe((isConfirmed) => {
                    // We get dialog result
                    if (isConfirmed) {
                        const counties = this.countyService.getAll().subscribe(x => {
                        this.counties = x;
                     });
                    }
                });
        }
    }

    public openCreate = () => {
        this.showDetails(new County(''));
    }


    public checkBoxOver() {
        this.ignoreClick = true;
    }

    public checkBoxOut() {
        this.ignoreClick = false;
    }

    public deleteAll = () => {
      const self = this;
      self.countyService.deleteAll(this.counties).subscribe(x => {
        this.countyService.getAll().subscribe(countries => {
          this.counties = countries;
        }, e => {
          console.log(e);
        });
      });
    }

    public isMarkedForDeletion(): boolean {
      let returnValue: boolean;
      returnValue = this.countyService.markForDeletion(this.counties);
      return returnValue;
    }
}
